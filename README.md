# Single Diffractive W analysis

# Analysis

The analysis is ran in SWAN from notebooks. The sequence is as follows

1. EventSelection.ipynb
1. NonPromptEstimation.ipynb
1. BackgroundValidation.ipynb


# NANO production

This requires lxplus, a certificate and a CMSSW installation

## Installation

```
cmsrel CMSSW_10_6_30_patch1
cd CMSSW_10_6_30_patch1/src
cmsenv

git clone git@github.com:michael-pitt/PPSTools.git
git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools
git clone ssh://git@gitlab.cern.ch:7999/psilva/sdanalysis.git UserCode/SDAnalysis
scram b -j
```

## MINIAOD

Before submitting you may want to edit the condor jdl file.

```
cd test/prod
voms-proxy-init --voms cms --rfc --valid 172:00 --out myproxy509
sh fillMinBiasFiles.sh
condor_submit condor.sub
```

## NANOAOD

For MC before running NANOAOD we run the direct simulation of protons in PPS and only after we run the NANOAOD and the skimming.
For the data the direct simulation is needed.
For MC the submission can be launched on condor with:

```
cd test
sh submitNano.sh
```

Once jobs are done you can merge/compress further the outputs with the following script:

```
sh runMergeNano.sh
```

In both cases you may want to edit input/output and other parameters in those files, before using them blindly. 

For Data, crab is to be used. In this case it suffices to start the proxy as usual and then

```
source /cvmfs/cms-ib.cern.ch/latest/cmsset_default.sh
crab crab_SingleMuon.py
```

Or follow the recipe described [here](https://cms-talk.web.cern.ch/t/crab-job-failed-with-exit-code-7002/32986/32) to avoid a bug with the default 
crab version and python2/3.


# Luminosity and pileup

Uncertainty of 1.7% from [LumiRecommendationsRun2](https://twiki.cern.ch/twiki/bin/view/CMS/LumiRecommendationsRun2#2017)

```
source /cvmfs/cms-bril.cern.ch/cms-lumi-pog/brilws-docker/brilws-env
brilcalc lumi --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json -u /pb -i python/postprocessing/etc/combined_RPIN_CMS_LOWMU.json
```

| Trigger | Luminosity [pb] |
| ===== | ===== |
| inc | 197.3 | 
| HLT_HIMu17_v* | 196.4 |
| HLT_HIEle20_WPLoose_Gsf_v* | 196.4 |

Pileup has an average of 2.5 computed with

```
pileupCalc.py -i python/postprocessing/etc/combined_RPIN_CMS_LOWMU.json --inputLumiJSON  /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/PileUp/pileup_latest.txt --calcMode observed --minBiasXsec 69200 --maxPileupBin 20 --numPileupBins 20 python/postprocessing/etc/pileup_histo_69200.root
```

The weights for the MC can be stored to be used with `correctionlib` with the following script

```
 python python/postprocessing/modules/pileupWeights.py python/postprocessing/etc/pileup_histo_69200.root /eos/cms/store/cmst3/group/top/sdanalysis/NANO/Wlnu_Pt12_TuneCP5_13TeV/NANO_0.root
```
