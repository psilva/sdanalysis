import FWCore.ParameterSet.Config as cms
from PhysicsTools.NanoAOD.protons_cff import *

def sdanalysis_datacustomize(process) :
  return sdanalysis_customize(process,True)

def sdanalysis_customize(process,data=False):
  
  """customizes the final nano to use in the analysis"""

  #build a table for particle flow candidates
  process.pfCandidatesTable = cms.EDProducer("SimpleCandidateFlatTableProducer",
                                             src = cms.InputTag("packedPFCandidates"),
                                             cut = cms.string(""),
                                             name = cms.string("PFCands"),
                                             doc = cms.string("particle flow candidates"),
                                             singleton = cms.bool(False), # the number of entries is variable
                                             extension = cms.bool(False), 
                                             variables = cms.PSet(  pt   = Var("pt",   float, precision=4),
                                                                    phi  = Var("phi",  float, precision=4),
                                                                    eta  = Var("eta",  float, precision=4),
                                                                    mass = Var("mass", float, precision=4),
                                                                    pdgId  = Var("pdgId", int, doc="PDG code assigned by the event reconstruction (not by MC truth)"),
                                                                    puppiWeight = Var("puppiWeight()", float, doc="Puppi weight",precision=4),
                                                                    puppiWeightNoLep = Var("puppiWeightNoLep()", float, doc="Puppi weight removing leptons",precision=4),
                                                                    pvAssocQuality = Var("pvAssociationQuality()", int, doc="primary vertex association quality"),
                                                                  )
                                           )
  #insert always the PF candidates table
  process.nanoAOD_step.insert(-1,process.pfCandidatesTable)
  
  #insert the proton table and a table of packedGenParticles if MC
  if not data:
    process.protonTables = protonTables
    process.nanoAOD_step.insert(-1,process.protonTables)

    #keep proton and all final state particles for the balancing
    process.finalGenParticles.select.extend( ['keep (status==1 && abs(pdgId)==2212 && abs(eta)>0)', 'keep status==1' ]  )

    """
    process.packedGenTable = cms.EDProducer("SimpleCandidateFlatTableProducer",
                                            src = cms.InputTag("packedGenParticles"),
                                            cut = cms.string(""),
                                            name = cms.string("PackedGen"),
                                            doc = cms.string("packed gen particles (status=1)"),
                                            singleton = cms.bool(False), # the number of entries is variable
                                            extension = cms.bool(False), 
                                            variables = cms.PSet(  pt   = Var("pt",   float, precision=4),
                                                                   phi  = Var("phi",  float, precision=4),
                                                                   eta  = Var("eta",  float, precision=4),
                                                                   mass = Var("mass", float, precision=4),
                                                                   pdgId  = Var("pdgId", int, doc="PDG code)"),
                                                                   statusFlags = process.genParticleTable.variables.statusFlags
                                                                 )
                                          )
    process.nanoAOD_step.insert(-1,process.packedGenTable)
    """

  return process
