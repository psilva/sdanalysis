drop *
keep run
keep luminosityBlock
keep event
keep Pass_*
keep *Lepton_*
keep *SelProton*
keep PFh*
keep Puppih*
keep PuppiNoLeph*
keep PuppiMET*
keep fixedGridRhoFastjetAll
keep PV_npvsGood
drop Gen*
drop nGen*

