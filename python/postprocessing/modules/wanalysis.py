#import pyspark
from dask_jobqueue import HTCondorCluster
from dask.distributed import Client
import ROOT
import itertools
from mccorrections import addCorrections

def genSelection(rdf: ROOT.RDataFrame, mc: bool) -> (ROOT.RDataFrame, bool):
    
    """
    generator level selection
    """
    if not mc: return rdf, []
    
    rdf = rdf.Define('GenLepton_W',   'genw_leptons(GenLepton_pdgId,GenLepton_pt,GenLepton_eta)') \
             .Define('nGenWLeptons',  'Sum(GenLepton_W)') \
             .Define('GenLepton_Z',   'z_leptons(GenLepton_pdgId,GenLepton_pt,GenLepton_eta,GenLepton_phi,GenLepton_W,false)') \
             .Define('nGenZLeptons',  'Sum(GenLepton_Z)') \
             .Define('GenCat',        'event_category(nGenWLeptons,nGenWLeptons,0,nGenZLeptons)') \
             .Define('GenLepton_sel', 'GenCat==23 ? GenLepton_Z : GenLepton_W') \
             .Define('nSelGenLeptons','Sum(GenLepton_sel)') \
             .Define('GenFlav',       'nSelGenLeptons>0 ? abs(GenLepton_pdgId[GenLepton_sel][0]) : 0') 
    
    #lead lepton variables
    rdf = rdf.Define('GenLepton1_q',   '(nSelGenLeptons>0 ? (GenLepton_pdgId[GenLepton_sel][0]<0 ? +1 : -1) : 0)') \
             .Define('GenLepton1_pt',  'nSelGenLeptons>0 ? GenLepton_pt[GenLepton_sel][0] : 0') \
             .Define('GenLepton1_eta', 'nSelGenLeptons>0 ? GenLepton_eta[GenLepton_sel][0] : 0') \
             .Define('GenLepton1_phi', 'nSelGenLeptons>0 ? GenLepton_phi[GenLepton_sel][0] : 0') \
             .Define('GenLepton1_px',  'nSelGenLeptons>0 ? GenLepton_pt[GenLepton_sel][0]*TMath::Cos(GenLepton_phi[GenLepton_sel][0]) : 0') \
             .Define('GenLepton1_py',  'nSelGenLeptons>0 ? GenLepton_pt[GenLepton_sel][0]*TMath::Sin(GenLepton_phi[GenLepton_sel][0]) : 0') \
             .Define('GenLepton1_pz',  'nSelGenLeptons>0 ? GenLepton_pt[GenLepton_sel][0]*TMath::SinH(GenLepton_eta[GenLepton_sel][0]) : 0')
    
    #trailer lepton variables
    rdf = rdf.Define('GenLepton2_q',   '(nSelGenLeptons>1 ? (GenLepton_pdgId[GenLepton_sel][1]<0 ? +1 : -1) : 0)') \
             .Define('GenLepton2_pt',  'nSelGenLeptons>1 ? GenLepton_pt[GenLepton_sel][1] : 0') \
             .Define('GenLepton2_eta', 'nSelGenLeptons>1 ? GenLepton_eta[GenLepton_sel][1] : 0') \
             .Define('GenLepton2_phi', 'nSelGenLeptons>1 ? GenLepton_phi[GenLepton_sel][1] : 0') \
             .Define('GenLepton2_px',  'nSelGenLeptons>1 ? GenLepton_pt[GenLepton_sel][1]*TMath::Cos(GenLepton_phi[GenLepton_sel][1]) : 0') \
             .Define('GenLepton2_py',  'nSelGenLeptons>1 ? GenLepton_pt[GenLepton_sel][1]*TMath::Sin(GenLepton_phi[GenLepton_sel][1]) : 0') \
             .Define('GenLepton2_pz',  'nSelGenLeptons>1 ? GenLepton_pt[GenLepton_sel][1]*TMath::SinH(GenLepton_eta[GenLepton_sel][1]) : 0')
    
    #neutrino pt,phi
    rdf = rdf.Define('GenNu_pt','sqrt(GenNu_px*GenNu_px+GenNu_py*GenNu_py)') \
             .Define('GenNu_phi','TMath::ATan2(GenNu_py,GenNu_px)') \
             .Define('GenNu_eta','TMath::ASinH(GenNu_pz/GenNu_pt)')
    
    #add also particle level z,w kinematics
    rdf = rdf.Define('p4_gendil', 'dileptonP4Vec(GenLepton1_pt,GenLepton1_eta,GenLepton1_phi,GenFlav,GenLepton2_pt,GenLepton2_eta,GenLepton2_phi,GenFlav)') \
             .Define('GenDilepton_pt', 'p4_gendil[0]') \
             .Define('GenDilepton_eta', 'p4_gendil[1]') \
             .Define('GenDilepton_phi', 'p4_gendil[2]') \
             .Define('GenDilepton_m', 'p4_gendil[3]') \
             .Define('GenW_mT', 'mT(GenLepton1_pt,GenLepton1_phi,GenNu_pt,GenNu_phi)') \
             .Define('GenW_pt', 'ptw(GenLepton1_pt,GenLepton1_phi,GenNu_pt,GenNu_phi)')
    
    #select one good proton
    rdf = rdf.Define('GoodGenProton_pos',   '(GenSelProton_sgn>0) && (GenSelProton_xi>0.025) && (GenSelProton_xi<0.15)') \
             .Define('GoodGenProton_neg',   '(GenSelProton_sgn<0) && (GenSelProton_xi>0.04) && (GenSelProton_xi<0.18)') \
             .Define('GoodGenProton',       'GoodGenProton_pos || GoodGenProton_neg') \
             .Define('nGoodGenProtons_pos', 'Sum(GoodGenProton_pos)') \
             .Define('nGoodGenProtons_neg', 'Sum(GoodGenProton_neg)') \
             .Define('nGoodGenProtons',     'Sum(GoodGenProton)') \
             .Define('GenProton_sgn',       'nGoodGenProtons>0 ? GenSelProton_sgn[GoodGenProton][0] : 0') \
             .Define('GenProton_xi',        'nGoodGenProtons>0 ? GenSelProton_xi[GoodGenProton][0] : 0') \
             .Define('GenProton_pz',        'nGoodGenProtons>0 ? GenSelProton_pz[GoodGenProton][0] : 0')

    gen_cols = ['genWeight','Pileup_nPU','L1PreFiringWeight.*',
                'GenCat','GenFlav','GenLepton1.*','GenLepton2.*','GenDilepton.*','GenW.*',
                'GenNu.*','Genb.*',
                'GenProton.*']

    return rdf, gen_cols


def leptonSelection(rdf: ROOT.RDataFrame, mc: bool) -> (ROOT.RDataFrame, bool):

    """
    performs the final selection of the leptons
    """
    #define lepton selections
    rdf = rdf.Define('Lepton_loose',  'loose_leptons(Lepton_pdgId,Lepton_pt,Lepton_eta,Lepton_id,Lepton_iso)') \
             .Define('Lepton_W',      'w_leptons(Lepton_pdgId,Lepton_pt,Lepton_eta,Lepton_id,Lepton_iso)') \
             .Define('Lepton_CRW',    'crw_leptons(Lepton_pdgId,Lepton_pt,Lepton_eta,Lepton_id,Lepton_iso)') \
             .Define('Lepton_Z',      'z_leptons(Lepton_pdgId,Lepton_pt,Lepton_eta,Lepton_phi,Lepton_W,false)') \
             .Define('nLooseLeptons', 'Sum(Lepton_loose)') \
             .Define('nWLeptons',     'Sum(Lepton_W)') \
             .Define('nCRWLeptons',   'Sum(Lepton_CRW)') \
             .Define('nZLeptons',     'Sum(Lepton_Z)') \
             .Define('Cat',           'event_category(nLooseLeptons, nWLeptons, nCRWLeptons, nZLeptons)') \
    
    #assign the final lepton selection flag
    rdf = rdf.Define('Lepton_sel', '(Cat==23 ? Lepton_Z : (Cat==24000 ? Lepton_CRW : Lepton_W ) )') \
             .Define('nSelLeptons', 'Sum(Lepton_sel)') \
             .Define('Flav', 'abs(Lepton_pdgId[Lepton_sel][0])') 
    
    #lead lepton variables
    rdf = rdf.Define('Lepton1_q',   '(nSelLeptons>0 ? (Lepton_pdgId[Lepton_sel][0]<0 ? +1 : -1): 0)') \
             .Define('Lepton1_pt',  'nSelLeptons>0 ? Lepton_pt[Lepton_sel][0] : 0') \
             .Define('Lepton1_eta', 'nSelLeptons>0 ? Lepton_eta[Lepton_sel][0] : 0') \
             .Define('Lepton1_phi', 'nSelLeptons>0 ? Lepton_phi[Lepton_sel][0] : 0') \
             .Define('Lepton1_px',  'nSelLeptons>0 ? Lepton_pt[Lepton_sel][0]*TMath::Cos(Lepton_phi[Lepton_sel][0]) : 0') \
             .Define('Lepton1_py',  'nSelLeptons>0 ? Lepton_pt[Lepton_sel][0]*TMath::Sin(Lepton_phi[Lepton_sel][0]) : 0') \
             .Define('Lepton1_pz',  'nSelLeptons>0 ? Lepton_pt[Lepton_sel][0]*TMath::SinH(Lepton_eta[Lepton_sel][0]) : 0') \
             .Define('Lepton1_iso', 'nSelLeptons>0 ? Lepton_iso[Lepton_sel][0] : 0')
   
    #trailer lepton variables
    rdf = rdf.Define('Lepton2_q',   '(nSelLeptons>1 ? (Lepton_pdgId[Lepton_sel][1]<0 ? +1 : -1) : 0)') \
             .Define('Lepton2_pt',  'nSelLeptons>1 ? Lepton_pt[Lepton_sel][1] : 0') \
             .Define('Lepton2_eta', 'nSelLeptons>1 ? Lepton_eta[Lepton_sel][1] : 0') \
             .Define('Lepton2_phi', 'nSelLeptons>1 ? Lepton_phi[Lepton_sel][1] : 0') \
             .Define('Lepton2_px',  'nSelLeptons>1 ? Lepton_pt[Lepton_sel][1]*TMath::Cos(Lepton_phi[Lepton_sel][1]) : 0') \
             .Define('Lepton2_py',  'nSelLeptons>1 ? Lepton_pt[Lepton_sel][1]*TMath::Sin(Lepton_phi[Lepton_sel][1]) : 0') \
             .Define('Lepton2_pz',  'nSelLeptons>1 ? Lepton_pt[Lepton_sel][1]*TMath::SinH(Lepton_eta[Lepton_sel][1]) : 0') \
             .Define('Lepton2_iso', 'nSelLeptons>1 ? Lepton_iso[Lepton_sel][1] : 0') \
    
    #add also z,w kinematics
    rdf = rdf.Define('p4_dil', 'dileptonP4Vec(Lepton1_pt,Lepton1_eta,Lepton1_phi,Flav,Lepton2_pt,Lepton2_eta,Lepton2_phi,Flav)') \
             .Define('Dilepton_pt', 'p4_dil[0]') \
             .Define('Dilepton_eta','p4_dil[1]') \
             .Define('Dilepton_phi','p4_dil[2]') \
             .Define('Dilepton_m',  'p4_dil[3]') \
             .Define('W_mT', 'mT(Lepton1_pt,Lepton1_phi,PuppiMET_pt,PuppiMET_phi)') \
             .Define('W_pt', 'ptw(Lepton1_pt,Lepton1_phi,PuppiMET_pt,PuppiMET_phi)')
    
    cols = ['Cat','Flav','nSelLeptons','Lepton1.*','Lepton2.*','Dilepton.*','W.*']
    return rdf, cols

    
def protonSelection(rdf: ROOT.RDataFrame, mc: bool) -> (ROOT.RDataFrame, bool):
    
    """
    performs the final selection of the protons
    """
    
    #select one good proton
    rdf = rdf.Define('GoodProton_pos',   '(SelProton_sgn>0) && (SelProton_xi>0.025) && (SelProton_xi<0.15)') \
             .Define('GoodProton_neg',   '(SelProton_sgn<0) && (SelProton_xi>0.04) && (SelProton_xi<0.18)') \
             .Define('GoodProton',       'GoodProton_pos || GoodProton_neg') \
             .Define('nGoodProtons_pos', 'Sum(GoodProton_pos)') \
             .Define('nGoodProtons_neg', 'Sum(GoodProton_neg)') \
             .Define('nGoodProtons',     'Sum(GoodProton)') \
             .Define('Proton_sgn',       'nGoodProtons>0 ? SelProton_sgn[GoodProton][0] : 0') \
             .Define('Proton_xi',        'nGoodProtons>0 ? SelProton_xi[GoodProton][0] : 0') \
             .Define('Proton_pz',        'nGoodProtons>0 ? SelProton_pz[GoodProton][0] : 0')

    proton_cols = ['nGoodProtons.*', 'Proton.*']
    return rdf, proton_cols


def recoilSelection(rdf: ROOT.RDataFrame, mc: bool) -> (ROOT.RDataFrame, bool):
    
    """
    performs the final selection of recoil estimators
    """
    
    #save met and balancing variables
    recoil_cols = ['PuppiMET.*','PFh.*','Puppih.*']
    return rdf, recoil_cols


def runWanalysis(input: list, output: str, mc: bool, correctionsjson: str, filterOn=None, distributed=False) -> None:
 
    """
    Select the W- or Z-like events and save a snapshot
    
        input := list of files to process
        output := output file name
        mc := use to steer MC or data-specific operations
        correctionsjson := json file describing corrections to compute
        filterOn := apply this filter string
        distributed := distribute to cluster
        
    The method assumes that the `python/postprocessing/modules/nano_helpers.h` has been pre-processed by ROOT, e.g. with
    
    ```ROOT.gROOT.ProcessLine('#include "python/postprocessing/modules/nano_helpers.h"')```
    
    """
    
    if not distributed:
        ROOT.ROOT.EnableImplicitMT()
        rdf = ROOT.RDataFrame("Events", input)
    else:
        #conf = SparkConf().setAppName(appName).setMaster(master)
        #sc = SparkContext(conf=conf)
        #RDataFrame = ROOT.RDF.Experimental.Distributed.Spark.RDataFrame
     
        cluster = HTCondorCluster(
            cores=1,
            memory='2000MB',
            disk='1000MB',
        )
        cluster.scale(4)
        client = Client(cluster)
        rdf = ROOT.RDataFrame("Events", input, daskclient=client)
        
    #define the selections and branches with an RDataFrame
    
    #selections
    rdf, gen_cols = genSelection(rdf,mc)
    rdf, lepton_cols = leptonSelection(rdf,mc)
    rdf, proton_cols = protonSelection(rdf,mc)
    rdf, recoil_cols = recoilSelection(rdf,mc)
            
    rdf = rdf.Filter('Cat>0 || GenCat>0' if mc else 'Cat>0')
    if not filterOn is None:
        rdf=rdf.Filter(filterOn)
        
    #save snapshot with interesting branches
    branch_names_list = ['run','luminosityBlock', 'event', 'fixedGridRhoFastjetAll','PV_npvsGood']
    branch_names_list += lepton_cols
    branch_names_list += proton_cols
    branch_names_list += recoil_cols
    branch_names_list += gen_cols
    if mc and not correctionsjson is None:
        rdf, sf_cols = addCorrections(rdf,correctionsjson)
        branch_names_list += sf_cols
    branchList='('+'|'.join(branch_names_list)+')'
    rdf.Snapshot('Events', output, branchList)
    
    #disable the implicit multi-threading
    if not distributed:
        ROOT.ROOT.DisableImplicitMT()


def main():
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input",
                        help='input nano=%(default)s',
                        default='/eos/cms/store/cmst3/group/top/sdanalysis/NANO-v2/Wlnu_pomflux_TuneCP5_lowPU_13TeV-pythia8/NANO_0.root')
    parser.add_argument("-o", "--output",
                        help='output file default=%(default)s',
                        default='test.root')
    parser.add_argument("--data",
                        help='process as data default=%(default)s',
                        default=False, action='store_true')
    parser.add_argument("--json",
                        help='json file describing corrections to apply=%(default)s',
                        default=None)
    parser.add_argument("--distributed",
                        help='distribute jobs to cluster=%(default)s',
                        default=False, action='store_true')

    args = parser.parse_args()
    
    ROOT.gROOT.ProcessLine('#include "python/postprocessing/modules/nano_helpers.h"')
    runWanalysis(input=args.input.split(','), 
                 output=args.output, 
                 mc=~args.data,
                 correctionsjson=args.json,
                 distributed=args.distributed)
    
if __name__ == '__main__':
    main()
