#!/usr/bin/env python

import os
import sys
import ROOT
import correctionlib.convert
import correctionlib.schemav2 as cs
import hist
import gzip

def generatePileupScaleFactors(mcurl: str,dataurl: str) -> str:
    
    """this function takes care of computing the data/MC ratio for the pileup distribution and generating a correction lib file
    """
    #get the reference data distribution
    datapu={}
    fIn=ROOT.TFile.Open(dataurl)
    datapu['Nom']=fIn.Get('pileup').Clone()
    datapu['Nom'].SetDirectory(0)
    xmin=datapu['Nom'].GetXaxis().GetXmin()
    xmax=datapu['Nom'].GetXaxis().GetXmax()
    nbins=datapu['Nom'].GetNbinsX()
    fIn.Close()
    print(f'Retrieved data pileup histogram')
    print(f"\t* average PU is {datapu['Nom'].GetMean():3.1f}")
    print(f"\t* #bins={nbins} spanning {xmin:3.0f}-{xmax:3.0f}")

    #get the MC
    df=ROOT.RDataFrame('Events',mcurl)
    mcpu=df.Histo1D( ('obspu','obspup',nbins,xmin,xmax), 'Pileup_nPU' )
    mcpu=mcpu.GetValue()
    print('Retrieved MC observed pileup')

    #normalize the main reference
    kfact=mcpu.Integral()/datapu['Nom'].Integral()
    datapu['Nom'].Divide(mcpu)
    datapu['Nom'].Scale(kfact)

    #do the same for the variations
    for xsec,tag in [('66000','Dn'),('72400','Up')]:
        fIn=ROOT.TFile.Open(dataurl.replace('69200',xsec))
        datapu[tag]=fIn.Get('pileup').Clone(f'pileup_{tag}')
        datapu[tag].SetDirectory(0)
        kfact=mcpu.Integral()/datapu[tag].Integral()
        datapu[tag].Divide(mcpu)
        datapu[tag].Scale(kfact)        
        fIn.Close()
    
    #define the correction and systematics
    edges=[datapu['Nom'].GetXaxis().GetBinLowEdge(xbin+1) for xbin in range(nbins)]+[xmax]
    puweight = cs.Correction(
        name="puweight",
        version=1,
        inputs=[
            cs.Variable(name="nPU",  type="real",    description="observed pileup"),
            cs.Variable(name="syst", type="string", description="Systematic variation: {Nom,Up,Dn}")
        ],
        output=cs.Variable(name="weight", type="real", description="Multiplicative event weight"),
        data=cs.Category(
            nodetype="category",
            input="syst",
            content=[
                cs.CategoryItem(key=key,
                                value=cs.Binning(nodetype="binning",
                                                 input="nPU",
                                                 edges=edges,
                                                 content=[datapu[key].GetBinContent(xbin+1) for xbin in range(nbins)],
                                                 flow='clamp')
                               )
                for key in datapu
            ]
        )
    )
                
    #save to file
    cset = correctionlib.schemav2.CorrectionSet(
        schema_version=2,
        description="pileup corrections",
        corrections=[puweight]
    )
    dirname=os.path.dirname(dataurl)
    pucorrurl=f"{dirname}/puWeights.json.gz"
    with gzip.open(pucorrurl, "wt") as fout:
        fout.write(cset.json(exclude_unset=True))
    print(f'Corrections stored in {pucorrurl}')
    import rich
    rich.print(puweight)
    
    return pucorrurl

def testPileupScaleFactors(pusfurl: str, obspu: float) -> bool :
    
    """this function tests the correction of pileup starting from a file
    """
    
    ceval = correctionlib.CorrectionSet.from_file(pusfurl)
    print(f'Weights for obs. PU={obspu}')
    for var in ['Nom','Up','Dn']:
        wgt=ceval['puweight'].evaluate(obspu,var)
        print(f'\t* {var} = {wgt:3.2f}')

def main():
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--mc",
                        help='input nano=%(default)s',
                        default='/eos/cms/store/cmst3/group/top/sdanalysis/NANO/Wlnu_Pt12_TuneCP5_13TeV/NANO_0.root')
    parser.add_argument("--data",
                        help='nominal pileup data distribution=%(default)s',
                        default='python/postprocessing/etc/pileup_histo_69200.root')
    parser.add_argument("--sf",
                        help='scale factors file=%(default)s',
                        default='python/postprocessing/etc/puWeights.json.gz')    
    parser.add_argument("--test",
                        help='test scale factors only=%(default)s',
                        default=False, action='store_true')
    args = parser.parse_args()
        
    pusfurl=args.sf
    if not args.test:
        pusfurl=generatePileupScaleFactors(args.mc,args.data)        
    testPileupScaleFactors(pusfurl,2.)
 

if __name__ == '__main__':
    main()
