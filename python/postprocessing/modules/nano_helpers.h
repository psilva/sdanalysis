#ifndef __nano_helpers_h__ 
#define __nano_helpers_h__ 

#include "TVector2.h"
#include "TMath.h"
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include <vector>

constexpr float e_mass = 0.000511;
constexpr float mu_mass = 0.10566;
constexpr float w_mass = 80.377;
constexpr float z_mass = 91.1876;

using namespace ROOT::VecOps; 

using rvec_f = const RVec<float>;
using rvec_i = const RVec<int>;
using rvec_b = const RVec<bool>;

/**
    @short return a list of loose leptons
    check python/postprocessing/modules/protonskimmer.py for id+iso codes
    muons: loose id+very loose iso
    electrons: loose id+iso
*/
rvec_b loose_leptons(const rvec_i &pdgId, const rvec_f &pt, const rvec_f &eta, const rvec_i &id, const rvec_f &iso) {
    
    std::vector<bool> loose_lepton(pdgId.size(),false);
    for(size_t i=0; i<pdgId.size(); i++){
        if( abs(pdgId[i])==13 ) {
            loose_lepton[i] = (pt[i]>20) && (fabs(eta[i])<2.4) && (id[i]>=4);
        } else {
            loose_lepton[i] = (pt[i]>20) && (fabs(eta[i])<2.5) && (id[i]>=16);
        }            
    }
        
    return rvec_b(loose_lepton.begin(), loose_lepton.end());
}


/**
    @short return a list of leptons selected with the W->lnu criteria
    check python/postprocessing/modules/protonskimmer.py for id+iso codes
    muons: tight id+iso
    electrons: tight id+iso
*/
rvec_b w_leptons(const rvec_i &pdgId, const rvec_f &pt, const rvec_f &eta, const rvec_i &id, const rvec_f &iso) {
    
    std::vector<bool> w_lepton(pdgId.size(),false);
    for(size_t i=0; i<pdgId.size(); i++){
        if( abs(pdgId[i])==13 ) {
            w_lepton[i] = (pt[i]>20) && (fabs(eta[i])<2.4) && (id[i]>=126);
        } else {
            w_lepton[i] = (pt[i]>20) && (fabs(eta[i])<2.5) && (id[i]>=256);
        }            
    }
        
    return rvec_b(w_lepton.begin(), w_lepton.end());
}  

/**
    @short return a list of leptons failing one of the final W criteria
    check python/postprocessing/modules/protonskimmer.py for id+iso codes
    muons: loose id only
    electrons: loose id only
*/
rvec_b crw_leptons(const rvec_i &pdgId, const rvec_f &pt, const rvec_f &eta, const rvec_i &id, const rvec_f &iso) {
    
    std::vector<bool> w_lepton(pdgId.size(),false);
    for(size_t i=0; i<pdgId.size(); i++){
        if( abs(pdgId[i])==13 ) {
            w_lepton[i] = (pt[i]>20) && (fabs(eta[i])<2.4) && (id[i]>=2);
        } else {
            w_lepton[i] = (pt[i]>20) && (fabs(eta[i])<2.5) && (id[i]>=2);
        }            
    }
        
    return rvec_b(w_lepton.begin(), w_lepton.end());
}  

/**
    @short return a list of leptons selected with the W->lnu criteria
*/
rvec_b genw_leptons(const rvec_i &pdgId, const rvec_f &pt, const rvec_f &eta) {
    
    std::vector<bool> w_lepton(pdgId.size(),false);
    for(size_t i=0; i<pdgId.size(); i++){
        w_lepton[i] = (abs(pdgId[i])==11 || abs(pdgId[i])==13) && (pt[i]>20) && (fabs(eta[i])<2.4);
    }
        
    return rvec_b(w_lepton.begin(), w_lepton.end());
} 

/**
    @short finds a Z->ll candidate
*/
rvec_b z_leptons(const rvec_i &pdgId, const rvec_f &pt, const rvec_f &eta, const rvec_f &phi, 
                 const rvec_b &isgood, bool req_offshell=false) {

    std::vector<bool> z_lepton(pdgId.size(),false);
    for(size_t i=0; i<pdgId.size(); i++) {
        
        int absid_i=abs(pdgId[i]);
        ROOT::Math::PtEtaPhiMVector p4_i(pt[i],eta[i],phi[i], absid_i==11 ? e_mass : mu_mass );

        for(size_t j=i+1; j<pdgId.size(); j++) {
            
            int absid_j=abs(pdgId[j]);
            
            //require same flavor
            if(absid_i!=absid_j)continue;
            
            //both must be loose and at least one have w quality
            int ngood=isgood[i]+isgood[j]; 
            if(ngood<2) continue;

            //mass constraint
            ROOT::Math::PtEtaPhiMVector p4_j(pt[j],eta[j],phi[j], absid_j==11 ? e_mass : mu_mass );
            float m = (p4_i+p4_j).mass();
            bool isonz( fabs(m-z_mass)<15 );
            if( isonz==req_offshell ) continue;
                        
            z_lepton[i]=true;
            z_lepton[j]=true;
        }
    }
    
    return rvec_b(z_lepton.begin(), z_lepton.end());
}    

/**
    @short a wrapper for z selection starting from the id/iso values directly
*/
rvec_b z_leptons_fromid(const rvec_i &pdgId, const rvec_f &pt, const rvec_f &eta, const rvec_f &phi, 
                 const rvec_i &id, const rvec_f &iso, bool req_offshell=false) {
    
    auto isw = w_leptons(pdgId,pt,eta,id,iso);
    return z_leptons(pdgId,pt,eta,phi,isw,req_offshell);
}

/**
    @short W-like candidate kinematics
*/
float mT(const float &pt1, const float &phi1, const float &pt2, const float &phi2) {
    float dphi = TVector2::Phi_mpi_pi(phi1-phi2);
    float mt = TMath::Sqrt(2*pt1*pt2*(1-TMath::Cos(dphi)));
    return mt;
}
float ptw(const float &pt1,const float &phi1,const float &pt2,const float &phi2) {      
    float px=pt1*cos(phi1)+pt2*cos(phi2);
    float py=pt1*sin(phi1)+pt2*sin(phi2);
    return sqrt(px*px+py*py);
}


/**
    @short Z-like candidate kinematics
*/
ROOT::Math::PtEtaPhiMVector dileptonP4(const float &pt1,const float &eta1,const float &phi1,const int &id1,
                                       const float &pt2,const float &eta2,const float &phi2,const int &id2) {
    ROOT::Math::PtEtaPhiMVector p4_1(pt1,eta1,phi1, abs(id1)==11 ? e_mass : mu_mass );
    ROOT::Math::PtEtaPhiMVector p4_2(pt2,eta2,phi2, abs(id2)==11 ? e_mass : mu_mass );
    ROOT::Math::PtEtaPhiMVector p4(p4_1+p4_2);
    return p4;
}

/**
    @short returns a 4-vector (pt,eta,phi,mass)
*/
rvec_f dileptonP4Vec(const float &pt1,const float &eta1,const float &phi1,const int &id1,
          const float &pt2,const float &eta2,const float &phi2,const int &id2) {
    auto p4=dileptonP4(pt1,eta1,phi1,id1,pt2,eta2,phi2,id2);
    return rvec_f{(float)p4.pt(),(float)p4.eta(),(float)p4.phi(),(float)p4.mass()};
}
    


/**
    @short assigns an event category code based on the counting of leptons with different quality
*/
int event_category(int nLooseLeptons, int nWLeptons, int nCRWLeptons, int nZLeptons) {
    if(nZLeptons==2) return 23;
    else if(nWLeptons>1) return 23000;
    else if(nLooseLeptons==1 && nWLeptons==1) return 24;
    else if(nWLeptons==0 && nCRWLeptons==1) return 24000;
    return 0;
}


/**
    @short return the parallel and transverse projections of the recoil (h) along a particle direction (phi)
*/
rvec_f projectVectorAlong(const float &h_pt, const float &h_phi, const float &phi) {
        
    //define the two vector
    TVector2 h(h_pt*cos(h_phi),h_pt*sin(phi));
    TVector2 e(cos(phi),sin(phi));
 
    //project parallel and perpendicular
    float u1=h.X()*e.X()+h.Y()*e.Y();
    float u2=h.X()*e.Y()-h.Y()*e.X();
           
    //return 
    return rvec_f{u1,u2};
}


#endif
