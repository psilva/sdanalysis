#!/usr/bin/env python

import os
import sys
import pandas as pd
import numpy as np
import itertools
import correctionlib.convert
import correctionlib.schemav2 as cs
import gzip

def buildCorrectorForTriggerSF(sfdict: dict) -> str:
    
    """this function takes care of declaring a correctionlib for the trigger scale factors
    """
    
    trigsf=[]
    for pdgId,url in sfdict.items():
        pdgId=int(pdgId)
        df=pd.read_csv(url,sep=',',header='infer')
        
        qedges=np.array([-2,0,2])
        
        ptedges=sorted(df['ptdn'].unique().tolist())
        ptedges.append( df['ptup'].max() )
        ptedges=np.array(ptedges)
        
        etaedges=sorted(df['etadn'].unique().tolist())
        etaedges.append( df['etaup'].max() )
        etaedges=np.array(etaedges)
        
        v=np.zeros( shape=(len(qedges)-1,len(ptedges)-1,len(etaedges)-1) )
        vup=np.zeros_like(v)
        vdn=np.zeros_like(v)
        for i,row in df.iterrows():
            qbin=np.digitize(row['q'],qedges)-1
            ptbin=np.digitize(row[['ptdn','ptup']].mean(),ptedges,right=False)-1
            etabin=np.digitize(row[['etadn','etaup']].mean(),etaedges,right=False)-1
                   
            v[qbin,ptbin,etabin]=row['sf']
            vup[qbin,ptbin,etabin]=v[qbin,ptbin,etabin]+row['sfunc']
            vdn[qbin,ptbin,etabin]=v[qbin,ptbin,etabin]-row['sfunc']
        vdict={'Nom':v,'Up':vup,'Dn':vdn}  
        
    
        trigsf.append( 
            cs.Correction(
                name=f"trigsf_{pdgId}",
                version=1,
                inputs=[
                    cs.Variable(name="q",   type="real",    description="charge"),
                    cs.Variable(name="pt",  type="real",    description="transverse momentum"),
                    cs.Variable(name="eta", type="real",    description="pseudo-rapidity"),
                    cs.Variable(name="syst", type="string", description="Systematic variation: {Nom,Up,Dn}")
                ],
                output=cs.Variable(name="weight", type="real", description="Multiplicative event weight"),
                data=cs.Category(
                    nodetype="category",
                    input="syst",
                    content=[
                        cs.CategoryItem(
                            key=key,
                            value=cs.MultiBinning(
                                nodetype="multibinning",
                                inputs=['q','pt','eta'],
                                edges=[qedges.tolist(),ptedges.tolist(),etaedges.tolist()],
                                content=vdict[key].ravel().tolist(),
                                flow='clamp'
                            )
                        )
                        for key in vdict
                    ]
                )
            )
        )
                
    #save to file
    cset = correctionlib.schemav2.CorrectionSet(
        schema_version=2,
        description="trigger corrections",
        corrections=trigsf
    )
    trigsfurl=f"triggerscalefactors.json.gz"
    with gzip.open(trigsfurl, "wt") as fout:
        fout.write(cset.json(exclude_unset=True))
    print(f'Corrections stored in {trigsfurl}')
    import rich
    rich.print(cset)
    
    return trigsfurl


def testTriggerSF(trigsfurl):
    
    print('Testing trigger SF')
    ceval = correctionlib.CorrectionSet.from_file(trigsfurl)
    for pdgId,q,syst in itertools.product([11,13],[-1.,1.],['Nom','Up','Dn']):
        wgt=ceval[f'trigsf_{pdgId}'].evaluate(q,25.,0.8,syst)
        print(f'\t* {pdgId} {q} {syst} = {wgt:3.2f}')


def main():
    
    import argparse
    parser = argparse.ArgumentParser()
    known, unknown_args = parser.parse_known_args()
        
    sfdict=dict( [a.split('=') for a in unknown_args] )
    trigsfurl=buildCorrectorForTriggerSF(sfdict)
    testTriggerSF(trigsfurl)
    print(f'All OK: trigger scale factors @ {trigsfurl}')
    
if __name__ == '__main__':
    main()
