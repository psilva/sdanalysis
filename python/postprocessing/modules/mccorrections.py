import os
import ROOT
import json
import re
import itertools
import correctionlib
correctionlib.register_pyroot_binding()

def declareCorrectors(correctionsjson,era='2017_UL',only=None):
    
    """Declares the correctors to ROOT. 
    Notice correctors can only be declared once per session. 
    `only` can be used to select a specific set of correctors to be initiated.
    This only needs to be done once per session"""
    
    with open(correctionsjson,'r') as fin:
        SFs_dict = json.load(fin)[era]

    for syst, syst_desc in SFs_dict.items():
        
        if not only is None:
            if not syst in only : continue
        
        url=syst_desc['url']
        if url[0] != '/':
            url=os.path.dirname(__file__).replace('python/postprocessing/modules',url)
        
        #if already existing, skip it
        try :
            getattr(ROOT,f'cset{syst}')
            print(f'Skipping previously declared cset{syst}')
            continue
        except:
            print(f'Declaring cset{syst}')
            
        ROOT.gInterpreter.Declare(f'auto cset{syst} = correction::CorrectionSet::from_file("{url}");')
        for c,cdesc in syst_desc['corrections'].items():
            ckey=cdesc['key']
            ckeyreg=ckey.replace('-','_')
            ROOT.gInterpreter.Declare(f'auto cset{syst}_{ckeyreg} = cset{syst}->at("{ckey}");')

def addCorrections(df,correctionsjson,era='2017_UL',declare=False):
        
    """Adds the necessary weights and corrections to the dataframe mostly by means of correctionlib 
    
    Parameters
    ----------
    df : RDataFrame, the dataframe
    correctionsjson : str, location of the json file containing a dict describing the corrections to apply
    era : str, the era to pick from the correctionsjson file
    declare: bool, if true it calls declare correctors

    Returns
    -------
    The updated dataframe
    
    """
    
    year=re.findall('(\d+)',era)[0]
    
    if declare:
        declareCorrectors(correctionsjson,era='2017_UL')
    
    #dict of scale factors and corrections
    #main key is the era, followed by a dict where the keys are objects
    #for each object two items are included:
    #* the url with the corrections to be read by correctionlib
    #* a list of corrections (name,type) for which a new weight is instantiated 
    with open(correctionsjson,'r') as fin:
        SFs_dict = json.load(fin)[era]

    sf_cols=[]
        
    #Pileup (as the path is relative, and this can be executed from who knows where, it needs to be corrected)
    pu=SFs_dict['PU']
    for c,cdesc in pu['corrections'].items():
        ckey=cdesc['key']
        for vname,vkey in cdesc['variations'].items():
            newsf=f'PU_{c}{vname}'
            sf_cols.append(newsf)
            df=df.Define(newsf,
                         f'csetPU_{ckey}->evaluate({{(float)Pileup_nPU,"{vkey}"}})')
    
    #muons
    muon=SFs_dict['Muon']
    url=muon['url']
    for c,cdesc in muon['corrections'].items():
        ckey=cdesc['key']
        for vname,vkey in cdesc['variations'].items():
            newsf=f'Muon_{c}{vname}'
            sf_cols.append(newsf)
            df=df.Define(newsf,
                         f'(Flav==13 && Cat==24) ?'
                         f'csetMuon_{ckey}->evaluate({{abs(Lepton1_eta), Lepton1_pt, "{vkey}"}}) :'
                         f'((Flav==13 && Cat==23) ?'
                         f'csetMuon_{ckey}->evaluate({{abs(Lepton1_eta), Lepton1_pt, "{vkey}"}})*'
                         f'csetMuon_{ckey}->evaluate({{abs(Lepton2_eta), Lepton2_pt, "{vkey}"}}) :'
                         f'1)')
            
    #electrons
    electron=SFs_dict['Electron']
    url=electron['url']
    for c,cdesc in electron['corrections'].items():
        ckey=cdesc['key']
        ckeyreg=ckey.replace('-','_')
        for vname,vkey in cdesc['variations'].items():
            newsf=f'Electron_{c}{vname}'
            sf_cols.append(newsf)
            df=df.Define(newsf,
                         f'(Flav==11 && Cat==24) ?'
                         f'csetElectron_{ckeyreg}->evaluate({{"{year}", "{vkey}", "Tight", abs(Lepton1_eta), Lepton1_pt}}) :'
                         f'((Flav==11 && Cat==23) ?'
                         f'csetElectron_{ckeyreg}->evaluate({{"{year}", "{vkey}", "Tight", abs(Lepton1_eta), Lepton1_pt}})*'
                         f'csetElectron_{ckeyreg}->evaluate({{"{year}", "{vkey}", "Tight", abs(Lepton2_eta), Lepton2_pt}}):'
                         f'1)')
            
    #trigger
    trigger=SFs_dict['Trigger']
    url=trigger['url']
    for c,cdesc in trigger['corrections'].items():
        ckey=cdesc['key']
        for vname,vkey in cdesc['variations'].items():
            newsf=f'{c}{vname}'
            sf_cols.append(newsf)
            pdgMatch=13 if 'Trig_Mu' in newsf else 11
            df=df.Define(newsf,
                         f'Flav=={pdgMatch} ? csetTrigger_trigsf_{pdgMatch}->evaluate({{(float)Lepton1_q,Lepton1_pt,Lepton1_eta,"{vkey}"}}) : 1')

    return df,sf_cols

def main():
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--json",
                        help='description of the corrections to load with correction lib default=%(default)s',
                        default='python/postprocessing/etc/mccorrections.json')
    args = parser.parse_args()
    
    ROOT.ROOT.EnableImplicitMT()
    
    #generate a sample as expected
    df = ROOT.RDataFrame(1000)
    df=df.Define('Pileup_nPU','gRandom->Poisson(2)') \
         .Define('PV_npvsGood','gRandom->Poisson(Pileup_nPU)+1') \
         .Define('Flav',"gRandom->Rndm() > 0.5 ? 13 : 11") \
         .Define('Cat',"gRandom->Rndm() > 0.80 ? 23 : 24") \
         .Define('genWeight','1.0')
    for i in [1,2]:
        df=df.Define(f"Lepton{i}_q", "gRandom->Rndm() > 0.5 ? 1 : -1") \
             .Define(f"Lepton{i}_pt", "20+gRandom->Exp(5)") \
             .Define(f"Lepton{i}_eta", "gRandom->Rndm()*2")
    df.Snapshot('Events','test_events.root','(.*)')
    
    #declare and compute corrections
    df=ROOT.RDataFrame('Events','test_events.root')
    df,sf_cols=addCorrections(df,args.json,declare=True)
    
    baseWgt=[c for c in sf_cols if not 'Up' in c and not 'Dn' in c] + ['genWeight']
    print('Baseline weight will be composed from:',baseWgt)

    #plot variations
    varWgtNames=[]
    varWgtList=[]
    for i,var in itertools.product( range(len(baseWgt)-1), ['Up','Dn'] ):

        #copy baseline and substitute one of the weights by the alternative
        varWgt=baseWgt.copy()
        varWgt[i]+=var
        
        #add to list
        varWgtNames.append(varWgt[i])
        varWgtList.append('*'.join(varWgt))
    print(f'{len(varWgtNames)} weight variations will be used in control plots')
    
    #define the weight variation
    baseWgtStr='*'.join(baseWgt)
    varWgtStr=','.join(varWgtList)
    df = df.Define('weight',f'(double){baseWgtStr}').Vary( "weight", f"ROOT::RVecD{{{varWgtStr}}}", varWgtNames)
    hists = [
        df.Filter('Cat==24 && Flav==11').Histo1D( ROOT.RDF.TH1DModel("ept","ept",50,20,200), "Lepton1_pt", "weight"),
        df.Filter('Cat==24 && Flav==13').Histo1D( ROOT.RDF.TH1DModel("mpt","mpt",50,20,200), "Lepton1_pt", "weight"),
        df.Filter('Cat==23 && Flav==11').Histo1D( ROOT.RDF.TH1DModel("e1pt","e1pt",50,20,200), "Lepton1_pt", "weight"),
        df.Filter('Cat==23 && Flav==11').Histo1D( ROOT.RDF.TH1DModel("e2pt","e2pt",50,20,200), "Lepton2_pt", "weight"),
        df.Filter('Cat==23 && Flav==13').Histo1D( ROOT.RDF.TH1DModel("m1pt","m1pt",50,20,200), "Lepton1_pt", "weight"),
        df.Filter('Cat==23 && Flav==13').Histo1D( ROOT.RDF.TH1DModel("m2pt","m2pt",50,20,200), "Lepton2_pt", "weight"),
        df.Histo1D( ROOT.RDF.TH1DModel("npv","npv",10,0,10),   "PV_npvsGood", "weight")
    ]
    histsVaried = [
        ROOT.RDF.Experimental.VariationsFor(h) for h in hists
    ]
    fOut=ROOT.TFile.Open('test_histos.root','RECREATE')
    for hmap in histsVaried:
        for k in hmap.GetKeys():
            hmap[k].Write()
    fOut.Close()
        
    #save snapshot
    df.Snapshot('Events','test_corrections.root','(.*)')
    
    ROOT.ROOT.DisableImplicitMT()

    
if __name__ == '__main__':
    main()
