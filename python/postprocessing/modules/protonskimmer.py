from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaR
from collections import namedtuple
import itertools
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

"""
The following skimmer requires 
* >=1 lepton (electron or muon)
*     muon: tight, pt>15, eta<2.5
*     electron: cut based veto-ID only, pt>15, EB or EE
From the command line one can test it as follows
python $CMSSW_BASE/src/PhysicsTools/NanoAODTools/scripts/nano_postproc.py localdata nano.root \
       -I UserCode.SDAnalysis.postprocessing.modules.protonskimmer  protonSkimmerImpl \
       --bo UserCode/SDAnalysis/python/postprocessing/etc/keep_and_drop_protons.txt
"""
class protonSkimProducer(Module):

    def __init__(self, trigger_dict):
        self.trigger_dict=trigger_dict
        self.hasGEN=True
        pass

    def beginJob(self):
        pass

    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):

        self.out = wrappedOutputTree

        #trigger branches
        #rename triggers of interest otherwise they'll be dropped
        for key,trig_list in self.trigger_dict.items():
            for t in trig_list:
                self.out.branch(t.replace('HLT_','Pass_'),'O')

        #lepton branches
        for tag in ['','Gen']:
            self.out.branch('n{}Leptons'.format(tag),'I')
            for col,dtype in [ ('pdgId','I'),
                               ('pt','F'),('eta','F'),('phi','F'),
                               ('id','I'),('iso','F'),('dz','F'),('dxy','F'),('ip3d','F') ]:           
                self.out.branch('{}Lepton_{}'.format(tag,col),dtype,lenVar='n{}Leptons'.format(tag))
        
        #neutrino branches
        for col in ['px','py','pz']:
            self.out.branch('GenNu_{}'.format(col),'F')
    
        #recoil branches
        for tag,h,col in itertools.product(['PF','Puppi','PuppiNoLep'],['h','hTK','hHEF'],['px','py','pz']):
            self.out.branch('{}{}_{}'.format(tag,h,col),"F")
        for h,col in itertools.product(['b','bTK','bHEF','bZDC'],['px','py','pz','sumpt','sumpz']):
            self.out.branch('Gen{}_{}'.format(h,col),"F")
    
        #proton branches
        for tag in ['','Gen']:
            self.out.branch("n{}SelProtons".format(tag), "I")
            for col in ['pz','xi','sgn','t','x','y']:
                self.out.branch("{}SelProton_{}".format(tag,col), "F", lenVar='n{}SelProtons'.format(tag))
                    
    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    def selectGEN(self, event):

        """generator level leptons are selected with pT>20 GeV within |eta|<2.5"""

        #fill branches for gen leptons
        leptons = Collection(event, "GenDressedLepton")
        sel_leptons=[l for l in leptons if l.pt>20 and abs(l.eta)<2.5]
        n_sel_leptons=len(sel_leptons)
        self.out.fillBranch('nGenLeptons',n_sel_leptons)
        if n_sel_leptons>0:
            for col in ['pt','eta','phi','pdgId']:
                vals = [getattr(l,col) for l in sel_leptons]
                self.out.fillBranch('GenLepton_{}'.format(col), vals)

        #other particles of interest: 
        #surving protons, neutrino sum, recoil estimators
        genParticles = Collection(event, 'GenPart')
        proton_cols=['pz', 'xi', 'sgn', 't', 'x', 'y']
        Proton = namedtuple('Proton', proton_cols)
        protons = []
        neutrino = ROOT.TVector3(0,0,0)
        hdict={}
        pzsumdict={}
        ptsumdict={}
        for h in ['b','bTK','bHEF','bZDC']:
            hdict[h]=ROOT.TVector3(0,0,0)
            pzsumdict[h]=0.
            ptsumdict[h]=0.
        for g in genParticles:
            
            if g.status !=1 : continue
            
            gid=abs(g.pdgId)
            p4=ROOT.Math.PtEtaPhiMVector(g.pt,g.eta,g.phi,g.mass)
            p3=ROOT.TVector3(p4.Px(),p4.Py(),p4.Pz())

            #proton
            genp=(gid==2212) and (abs(g.eta)>0) and (g.statusFlags==8193)
            if genp:
                pz=p4.Pz()
                psgn=1 if pz>0 else -1
                pbeam=6500.
                pini=ROOT.Math.PxPyPzMVector(0.,0.,psgn*pbeam,g.mass)
                t=(pini-p4).M2()
                xi=1 - p4.E()/pini.E()
                protons.append(
                    Proton(pz,xi,1 if pz>0 else -1, t, 0, 0)
                )

            #neutrinos from the hard process
            gennu=(gid in [12,14,16])
            if gennu: 
                flags=g.statusFlags
                isPrompt = (flags & (1<<0))>0
                isHardProcess = (flags & (1<<7))>0
                fromHardProcess = (flags & (1<<8))>0
                isHardProcessTauDecayProduct = (flags & (1<<9))>0
                isDirectHardProcessTauDecayProduct = (flags & (1<<10))>0
                selectnu=isPrompt or isHardProcess or fromHardProcess or isHardProcessTauDecayProduct or isDirectHardProcessTauDecayProduct
                if selectnu:
                    neutrino += p3

            #all particles
            pz=abs(p3.Pz())
            pt=p3.Pt()
            hdict['b'] += p3
            pzsumdict['b'] += pz 
            ptsumdict['b'] += pt
            if abs(g.eta)<4.7:
                regTag='TK' if abs(g.eta)<2.5 else 'HEF'
                hdict['b'+regTag] += p3
                pzsumdict['b'+regTag] += pz 
                ptsumdict['b'+regTag] += pt
            elif abs(g.eta)>8.5 and (gid in [22,2112]): #neutrons and photons only
                hdict['bZDC'] += p3
                pzsumdict['bZDC'] += pz 
                ptsumdict['bZDC'] += pt

        #proton branches
        n_sel_protons=len(protons)
        self.out.fillBranch("nGenSelProtons",n_sel_protons)
        if n_sel_protons>0:
            for col in proton_cols:
                vals = [getattr(p,col) for p in protons]
                self.out.fillBranch("GenSelProton_{}".format(col), vals)
            
        #neutrino branches
        for col in ['px','py','pz']:            
            self.out.fillBranch('GenNu_{}'.format(col), getattr(neutrino,col.capitalize())() )

        #balancing branches
        for h,col in itertools.product( hdict.keys(), ['px','py','pz'] ):
            self.out.fillBranch('Gen{}_{}'.format(h,col), getattr(hdict[h],col.capitalize())() )
        for h in pzsumdict.keys():
            self.out.fillBranch('Gen{}_sumpt'.format(h),  ptsumdict[h])
            self.out.fillBranch('Gen{}_sumpz'.format(h),  pzsumdict[h])

        return {'n_sel_leptons':n_sel_leptons,'n_sel_protons':n_sel_protons}


    def fillRecoil(self,event):

        #init the estimators
        hdict={}
        for tag,h in itertools.product(['PF','Puppi','PuppiNoLep'],['h','hTK','hHEF']):
            hdict[tag+h]=ROOT.TVector3(0,0,0)

        #sum up the estimators
        pfcands = Collection(event, "PFCands")
        for p in pfcands:
            regTag='TK' if abs(p.eta)<2.5 else 'HEF'             
            p3=ROOT.TVector3(p.pt*ROOT.TMath.Cos(p.phi),p.pt*ROOT.TMath.Sin(p.phi),p.pt*ROOT.TMath.SinH(p.eta))
            hdict['PFh'] += p3
            hdict['PFh'+regTag] += p3 
            p4puppi=p.puppiWeight*p3
            hdict['Puppih'] += p4puppi
            hdict['Puppih'+regTag] += p4puppi
            p4puppinolep=p.puppiWeightNoLep*p3
            hdict['PuppiNoLeph'] += p4puppinolep
            hdict['PuppiNoLeph'+regTag] += p4puppinolep

        #fill the branches
        for tag,h,col in itertools.product(['PF','Puppi','PuppiNoLep'],['h','hTK','hHEF'],['px','py','pz']):
            self.out.fillBranch('{}{}_{}'.format(tag,h,col), getattr(hdict[tag+h],col.capitalize())() )


    def selectRECO(self, event):

        report = {}
        report['n_trig_bits'], triggers_fired = self.selectTriggerBits(event)
        report['n_sel_leptons'] = self.selectLeptons(event)
        report['n_sel_protons'] = self.selectProtons(event)
        self.fillRecoil(event)

        return report

    def selectTriggerBits(self,event):

        """takes care of selecting the needed trigger bits and filling the pass flags"""
        
        #check which triggers fired
        trigger_fired_dict={}
        for key,trig_list in self.trigger_dict.items():
            tbits=[]
            for trig in trig_list:
                pass_trig = getattr(event,trig) if hasattr(event,trig) else False
                tbits.append(pass_trig)
                self.out.fillBranch(trig.replace('HLT_','Pass_'),bool(pass_trig))
            trigger_fired_dict[key] = tbits.count(True)

        nfire=sum([val for val in trigger_fired_dict.values()])

        return nfire, trigger_fired_dict

    def selectLeptons(self,event):

        """takes care of selecting the lepton candidates in RECO and filling the appopriate branches"""

        lepton_cols=['pdgId', 'pt', 'eta', 'phi', 'id', 'iso', 'dz', 'dxy', 'ip3d']
        Lepton = namedtuple('Lepton', lepton_cols)
        sel_leptons = []
        
        #pre-select only the trigger objects matched to leptons
        trigObjs = Collection(event, "TrigObj")    
        lepTrigObjEtaPhi={11:[],13:[]}
        for to in trigObjs:
            absid=abs(to.id)
            if not absid in [11,13] : continue
            lepTrigObjEtaPhi[absid].append( (to.eta,to.phi) )

        #muon selector
        muons = Collection(event, "Muon")    
        for m in muons:
            if m.pt<20 : continue
            if abs(m.eta)>2.5 : continue
            matchesTO=[ deltaR(to[0],to[1],m.eta,m.phi)<0.1 for to in lepTrigObjEtaPhi[13] ]
            matchesTriggerObject = (sum(matchesTO)>0)
            mid=self.myMuonCategories(matchesTriggerObject,m.looseId,m.mediumId,m.tightId,m.pfRelIso04_all)
            if mid<2 : continue  #require loose ID-only muons
            sel_leptons.append(
                Lepton(-13*m.charge,m.pt,m.eta,m.phi,mid,m.pfRelIso04_all,m.dz,m.dxy,m.ip3d)
            )
            
        #electron selector
        electrons = Collection(event, "Electron")
        for e in electrons:
            if e.pt<20 : continue
            scEta=e.eta+e.deltaEtaSC
            if abs(scEta)>2.5 or (abs(scEta)>1.4442 and abs(scEta)<1.566) : continue
            matchesTO=[ deltaR(to[0],to[1],e.eta,e.phi)<0.1 for to in lepTrigObjEtaPhi[13] ]
            matchesTriggerObject = (sum(matchesTO)>0)
            eid = self.myElectronCategories(matchesTriggerObject,e.vidNestedWPBitmap)
            if eid<2: continue #require veto ID-only electrons
            sel_leptons.append(
                Lepton(-11*e.charge,e.pt,e.eta,e.phi,eid,e.pfRelIso03_all,e.dz,e.dxy,e.ip3d)
            )

        #fill the branches
        n_sel_leptons = len(sel_leptons)
        self.out.fillBranch('nLeptons',n_sel_leptons)
        for col in lepton_cols:
            vals = [getattr(l,col) for l in sel_leptons]
            self.out.fillBranch('Lepton_{}'.format(col),vals)

        return n_sel_leptons

    
    def myMuonCategories(self,matchesTriggerObject,isLoose,isMedium,isTight,pfRelIso04):

        """builds a custom made muon categorizer:
        b'0 - matches trigger object
        b'1 - loose
        b'2 - loose+very loose iso 
        b'3 - medium
        b'4 - medium+medium iso 
        b'5 - tight
        b'6 - tight + tight iso
        """

        #isolation https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideMuonIdRun2#Particle_Flow_isolation
        isVeryLooseIso = (pfRelIso04<0.40)
        isMediumIso = (pfRelIso04<0.20)
        isTightIso = (pfRelIso04<0.15)
        
        #build the muon code
        mcode=0
        muoncodebits=[matchesTriggerObject,
                      isLoose,
                      isLoose and isVeryLooseIso,
                      isMedium,
                      isMedium and isMediumIso,
                      isTight,
                      isTight and isTightIso]
        for ibit,ival in enumerate(muoncodebits):
            if not ival: continue
            mcode += (1<<ibit)

        return mcode

    
    
    def myElectronCategories(self,matchesTriggerObject,vidNestedWPBitmap):

        """inspects individually the VID cuts and builds the following set of codes:
        b'0 - matches trigger object
        b'1 - veto (except iso)
        b'2 - veto 
        b'3 - loose (except iso)
        b'4 - loose
        b'5 - medium (except iso)
        b'6 - medium
        b'7 - tight (except iso)
        b'8 - tight
        """
        
        vidnamelist = ['GsfEleMissingHitsCut',
                       'GsfEleConversionVetoCut',
                       'GsfEleRelPFIsoScaledCut',
                       'GsfEleEInverseMinusPInverseCut',
                       'GsfEleHadronicOverEMEnergyScaledCut',
                       'GsfEleFull5x5SigmaIEtaIEtaCut',
                       'GsfEleDPhiInCut',
                       'GsfEleDEtaInSeedCut',
                       'GsfEleSCEtaMultiRangeCut',
                       'MinPtCut']

        #get 3 bit code per cut applied
        vidvals=[]
        for i in range(len(vidnamelist)):
            val = ((vidNestedWPBitmap >> i*3) & 0x7)
            vidvals.append(val)


        #build the electron code
        ecode=1 if matchesTriggerObject else 0
        for idval in range(1,5):

            hasid,hasidexceptiso=True,True            

            for n,v in zip(vidnamelist,vidvals):
                hasid &= (v>=idval)
                if n!='GsfEleRelPFIsoScaledCut':
                    hasidexceptiso &= (v>=idval)

            if hasidexceptiso:
                bshift=idval*2-1
                ecode += (1<<bshift)
            if hasid:
                bshift=idval*2
                ecode += (1<<bshift)

            #not worth testing tighter working poinyd
            if not hasidexceptiso and not hasid:
                break

        return ecode
                

    def selectProtons(self, event):

        """takes care of selecting the proton candidates in RECO and filling the appopriate branches"""
              
        protons = Collection(event, "Proton_multiRP")
        tracks = Collection(event, "PPSLocalTrack")

        proton_cols=['pz', 'xi', 'sgn', 't', 'x', 'y']
        Proton = namedtuple('Proton', proton_cols)
        sel_protons = []

        for idx, pr in enumerate(protons):

            sgn = 1 if pr.arm==0 else -1
            pz = sgn*6500*pr.xi
            t = pr.t
            xi = pr.xi

            #find associated tracks:
            x,y=-99,-99
            for tr in tracks:
              if idx != tr.multiRPProtonIdx: continue
              x=tr.x
              y=tr.y

            sel_protons.append(Proton(pz,xi,sgn,t,x,y))
        
        n_sel_protons = len(sel_protons)
        self.out.fillBranch("nSelProtons", n_sel_protons)
        for col in proton_cols:
            vals = [getattr(p,col) for p in sel_protons]
            self.out.fillBranch("SelProton_{}".format(col), vals)
            
        return n_sel_protons

        
    def analyze(self, event):

        """process event, return True (go to next module) or False (fail, go to next event)"""

        if self.hasGEN:
            try:
                gen_report=self.selectGEN(event)
            except Exception as e:
                print("Failed to process GEN in first event, assuming it's data")
                print(e)
                self.hasGEN = False
                
        reco_report = self.selectRECO(event)      
        passRECO = (reco_report['n_trig_bits']>0) and (reco_report['n_sel_leptons']>0)

        passGEN = (gen_report['n_sel_leptons']>0) if self.hasGEN else False

        return passRECO or passGEN

    
dataProtonSkimmerImpl = lambda: protonSkimProducer(trigger_dict={13:['HLT_HIMu17'],11:['HLT_HIEle20_WPLoose_Gsf']})
mcProtonSkimmerImpl = lambda: protonSkimProducer(trigger_dict={13:['HLT_Mu17'],11:['HLT_Ele20_WPLoose_Gsf']})
