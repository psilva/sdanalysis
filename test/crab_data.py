from CRABClient.UserUtilities import config
config = config()

config.General.requestName = ''
config.General.workArea = 'crab_projects'
config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'NANO_data_cfg.py'
config.JobType.scriptExe = 'custom_crab.sh'
config.JobType.inputFiles = ['custom_crab.py','../python/postprocessing/etc/keep_and_drop_protons_data.txt']
config.JobType.scriptArgs = []
config.JobType.allowUndistributedCMSSW = True
config.JobType.outputFiles = ['nano_Skim.root']

config.Data.inputDataset = ''
config.Data.allowNonValidInputDataset = True

config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 30
config.Data.outLFNDirBase = '/store/group/cmst3/group/top/sdanalysis/Data'
config.Data.publication = False
config.Data.outputDatasetTag = ''
config.Data.lumiMask = '../python/postprocessing/etc/combined_RPIN_CMS_LOWMU.json'

config.Site.storageSite = 'T2_CH_CERN'
