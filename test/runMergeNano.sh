#!/bin/bash

indir=/eos/user/p/psilva/data/sdanalysis/NANO
whoami=`whoami`

samples=(`ls ${indir}`)
for s in ${samples[@]}; do
    localdir=/tmp/${whoami}/${s}
    mkdir -p ${localdir};
    python mergeNanoOutputs.py -i ${indir}/${s}/Chunks -o ${localdir} -t 'nano';
    mv -v ${localdir}/*.root ${indir}/${s};
    rm -rf ${localdir};
done
