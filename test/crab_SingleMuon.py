import sys
sys.path.append("./")
from crab_data import config

config.Data.inputDataset = '/SingleMuon/Run2017H-UL2017_MiniAODv2-v1/MINIAOD'

#v1
config.General.requestName = 'SingleMuon2017H'
config.Data.outputDatasetTag = config.General.requestName

#config.Site.blacklist = ['T1_US_FNAL']
