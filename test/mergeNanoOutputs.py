import os
import sys
import argparse
import subprocess

def mapNanosInChunks(path,targetchunksize,tag=None):
    
    """lists ROOT files in a path and groups them approximately by targetchunksize in GB"""
    
    chunks_dict={0:{'size':0,'files':[]}}
    chunk_idx=0
    for r, d, f in os.walk(path):
        for fp in f:
            if not '.root' in fp: continue
            if not tag is None and not tag in fp: continue
            fp=os.path.join(r, fp)
            fs=float(os.path.getsize(fp)) * 1e-9
            chunks_dict[chunk_idx]['size']+=fs
            chunks_dict[chunk_idx]['files'].append(fp)            
            if chunks_dict[chunk_idx]['size']>targetchunksize or len(chunks_dict[chunk_idx]['files'])>999:
                chunk_idx+=1
                chunks_dict[chunk_idx]={'size':0,'files':[]}

    return chunks_dict

def main():

    parser = argparse.ArgumentParser(description='hadd nanos')
    parser.add_argument('-i', '--input',     help='input directory with files',    default=None, type=str, required=True)
    parser.add_argument('-t', '--tag',       help='search for this pattern in the files',    default='nano', type=str, required=False)
    parser.add_argument('-f', '--force',     help='force merge even if already existing', default=False, action='store_true', required=False)
    parser.add_argument('-o', '--output',    help='output directory with files',   default=None, type=str, required=True)
    parser.add_argument('-s', '--chunkSize', help='size of the output chunk (Gb)', default=5, type=float)
    parser.add_argument(      '--dryRun',    help="build tasks don't do anything", action='store_true',  default=False)
    args = parser.parse_args()

    #build the merge map
    merge_dict=mapNanosInChunks(args.input,args.chunkSize,args.tag)
    print('Will hadd chunks in {} tasks'.format(len(merge_dict)))

    nerr=0
    for k,t in merge_dict.items():

        flist=t['files']
        print('Chunk {} has {} input files'.format(k,len(flist)))
        outfile='NANO_{}.root'.format(k)

        if not args.force and os.path.isfile('{}/{}'.format(args.output,outfile)):
            print('Chunk {} already exists for {}... pass'.format(k,args.tag))
            continue

        cmd='haddnano.py {out} {chunks} && mv -v {out} {store}/{out}'.format(out=outfile, 
                                                                             chunks=' '.join(flist),
                                                                             store=args.output)
        if args.dryRun:
            print(cmd)
        else:
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            if len(err)>0:
                print('Error occurred for ',cmd)
                print(err)
                nerr+=1
            else:
                fs=float(os.path.getsize('{}/{}'.format(args.output,outfile))) * 1e-9
                print('\t * Chunk #{} : {} has {:3.2f} Gb'.format(k,outfile,fs))

    if not args.dryRun and nerr==0:
        print('No error has been detected while merging!')
        print('You can now delete the original chunks with the following command')
        print('rm -rf {}'.format(args.input))

if __name__ == "__main__":
    sys.exit(main())

