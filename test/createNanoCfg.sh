#!/bin/bash

#MC
GT=106X_mc2017_realistic_v9For2017H_v1
ERA=Run2_2017,run2_nanoAOD_106Xv2
cmsDriver.py NANO --python_filename NANO_mc_cfg.py -s NANO --mc --conditions ${GT} \
    --era ${ERA} --eventcontent NANOAODSIM --datatier NANOAODSIM \
    --customise UserCode/SDAnalysis/myNANOcustomize_cfi.sdanalysis_customize \
    --customise_commands="process.add_(cms.Service('InitRootHandlers', EnableIMT = cms.untracked.bool(False)));process.MessageLogger.cerr.FwkReport.reportEvery=1000;" \
    --filein file:miniAOD_withProtons.root --fileout nano.root \
    -n -1 --no_exec || exit $? ;


#DATA
GT=106X_dataRun2_v37
ERA=Run2_2017,run2_nanoAOD_106Xv2
cmsDriver.py NANO --python_filename NANO_data_cfg.py -s NANO --data --conditions ${GT} \
    --era ${ERA} --eventcontent NANOAOD --datatier NANOAOD \
    --customise UserCode/SDAnalysis/myNANOcustomize_cfi.sdanalysis_datacustomize \
    --customise_commands="process.add_(cms.Service('InitRootHandlers', EnableIMT = cms.untracked.bool(False)));process.MessageLogger.cerr.FwkReport.reportEvery=1000;" \
    --filein miniaod.root --fileout nano.root \
    -n -1 --no_exec;
