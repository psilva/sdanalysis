#!/usr/bin/env python
import os
from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import *

# this takes care of converting the input files from CRAB
from PhysicsTools.NanoAODTools.postprocessing.framework.crabhelper import runsAndLumis

from UserCode.SDAnalysis.postprocessing.modules.protonskimmer import *
p = PostProcessor(".",
                  ['nano.root'],
                  modules=[dataProtonSkimmerImpl()],
                  provenance=True,
                  fwkJobReport=False,
                  jsonInput=runsAndLumis(),
                  outputbranchsel='keep_and_drop_protons_data.txt')
p.run()

print("DONE")
