import os
import sys
import htcondor
import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "--dryRun", action='store_true', help='do not run jobs')
parser.add_argument( "-i", "--input",  help='input jdl file')
args = parser.parse_args()

with open(args.input,'r') as jdl:
    job=jdl.read()

sub=htcondor.Submit(job)

tasks=[]

for i,j in enumerate(sub.jobs()):
    outfile=os.path.join(j['OutputDestination'],j['TransferOutput'])
    outfile=outfile.replace('root://eosuser.cern.ch/','')
    if os.path.isfile(outfile): continue
    tasks.append( (j['Cmd'],j['Args'],outfile) )
    
print('{} tasks need to be re-run'.format(len(tasks)))

def _runMissing(args):
    
    cmdargs=args[1].split()
    infile=cmdargs[0]
    outfile=args[2]
    localoutfile=os.path.basename(infile).replace('.root','_Skim.root')
    
    cmd='sh {} {}'.format(args[0],args[1])
    os.system(cmd)    
    os.system('mv -v {} {}'.format(localoutfile,outfile))
    
    return True

if not args.dryRun:
    from multiprocessing import Pool
    pool = Pool(8)
    pool.map(_runMissing,tasks)
else:
    print(f'JDL is {args.input}')
    print('Tasks will produce the following files')    
    for t in tasks:
        print(os.path.basename(t[-1]))
