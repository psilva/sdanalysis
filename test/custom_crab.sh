echo "================== START OF CUSTOM CRAB SCRIPT========================="

echo "Run CMSSW"
cmsRun -j FrameworkJobReport.xml -p PSet.py

echo "Run postprocessing"
python custom_crab.py

echo "============= END OF CUSTOM CRAB SCRIPT ========================="
