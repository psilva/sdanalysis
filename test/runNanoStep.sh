#!/bin/bash

WORKDIR=`pwd`
INPUT=${1}
ANGLE=${2}
CMSSW_BASE=${3}
PROCID=${4}

cd ${CMSSW_BASE}
source /cvmfs/cms.cern.ch/cmsset_default.sh
eval `scram runtime -sh`
cd ${WORKDIR}


if [ "${ANGLE}" != "-1" ]; then

    echo "Run on MC"
    
    #run direct proton sim
    echo "Add protons to MINIAOD"
    cmsRun ${CMSSW_BASE}/src/PPSTools/NanoTools/test/addProtons_miniaod.py inputFiles=${INPUT} doSignalOnly=True xangle=${ANGLE}
    
    # cmsDriver command for NANO
    echo "Run NANO step"
    GT=106X_mc2017_realistic_v9For2017H_v1
    ERA=Run2_2017,run2_nanoAOD_106Xv2
    cmsDriver.py NANO --python_filename ${WORKDIR}/NANO_mc_cfg.py -s NANO --mc --conditions ${GT} \
        --era ${ERA} --eventcontent NANOAODSIM --datatier NANOAODSIM \
        --customise UserCode/SDAnalysis/myNANOcustomize_cfi.sdanalysis_customize \
        --customise_commands="process.add_(cms.Service('InitRootHandlers', EnableIMT = cms.untracked.bool(False)));process.MessageLogger.cerr.FwkReport.reportEvery=1000;" \
        --filein file:miniAOD_withProtons.root --fileout ${WORKDIR}/nano.root \
        -n -1 --no_exec || exit $? ;
    cmsRun ${WORKDIR}/NANO_mc_cfg.py

    #run skimmer
    echo "run skim on MC"
    python ${CMSSW_BASE}/src/PhysicsTools/NanoAODTools/scripts/nano_postproc.py ${WORKDIR} ${WORKDIR}/nano.root \
        -I UserCode.SDAnalysis.postprocessing.modules.protonskimmer  mcProtonSkimmerImpl \
        --bo ${CMSSW_BASE}/src/UserCode/SDAnalysis/python/postprocessing/etc/keep_and_drop_protons.txt

    mv -v nano_Skim.root nano_Skim_${PROCID}.root

else

    #run skimmer
    echo "run skim on Data"
    python ${CMSSW_BASE}/src/PhysicsTools/NanoAODTools/scripts/nano_postproc.py ${WORKDIR} ${INPUT} \
        -I UserCode.SDAnalysis.postprocessing.modules.protonskimmer  dataProtonSkimmerImpl \
        --bo ${CMSSW_BASE}/src/UserCode/SDAnalysis/python/postprocessing/etc/keep_and_drop_protons_data.txt

    mv -v nano*.root nano_Skim_${PROCID}.root

fi
