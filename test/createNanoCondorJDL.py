import os

def main():

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input",
                        help='input directory default=%(default)s',
                        default=None)
    parser.add_argument("--xangle",
                        help='Beam crossing angle [murad] default=%(default)s',
                        default=-1)
    parser.add_argument("-o", "--output",
                        help='output directory default=%(default)s',
                        default=None)
    parser.add_argument("-s", "--submit",
                        help='submit=%(default)s',
                        default=False, action='store_true')
    parser.add_argument("-c", "--cmssw",
                        help='cmssw base default=%(default)s',
                        default='/afs/cern.ch/user/p/psilva/work/LowPU/CMSSW_10_6_30_patch1/src')
    args = parser.parse_args()

    #assume the samples are at the head of the input directory
    samples=[f for f in os.listdir(args.input) if os.path.isdir(os.path.join(args.input,f))]
    print('Creating condor jdl for {} samples'.format(len(samples)))

    #get list of files for the sample
    for s in samples:

        flist=[]
        for root, dirs, files in os.walk(os.path.join(args.input,s), topdown=False):
            for name in files:
                if 'Skim' in name: continue
                if not '.root' in name: continue
                flist.append('root://eosuser.cern.ch/'+os.path.join(root, name))
        print('Sample {} has {} files'.format(s,len(flist)))

        #build the condor script
        condorurl='condor_{}.sub'.format(s)
        condor=open(condorurl,'w')
        condor.write("tag         = {}\n".format(s))
        condor.write("cmssw_base  = {}\n".format(args.cmssw))
        condor.write("executable  = $(cmssw_base)/UserCode/SDAnalysis/test/runNanoStep.sh\n")
        condor.write("eosout      = {}\n".format(args.output))
        condor.write("arguments   = $(miniaod) {} $(cmssw_base) $(ProcId)\n".format(args.xangle))
        condor.write("transfer_output_files = nano_Skim_$(ProcId).root\n")
        condor.write("output_destination = root://eosuser.cern.ch/$(eosout)/$(tag)/Chunks\n")
        condor.write("MY.XRDCP_CREATE_DIR = True\n")
        condor.write("output      = job_$(ClusterId).out\n")
        condor.write("error       = job_$(ClusterId).err\n")
        condor.write("log         = job_$(ClusterId).log\n")        
        condor.write("\n")
        condor.write('+JobFlavour = "testmatch"\n')
        condor.write('+AccountingGroup = "group_u_CMST3.all"\n')
        condor.write('MY.WantOS   = "el7"\n')
        condor.write("\n")
        condor.write("queue miniaod from (\n")
        for f in flist:
            condor.write("  {}\n".format(f))
        condor.write(")\n")
        condor.close()

        if args.submit:
            os.system('condor_submit {}'.format(condorurl))

if __name__ == "__main__":
    main()
