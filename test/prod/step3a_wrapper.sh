#!/bin/bash

WORKDIR=`pwd`
SEED=$1
MINBIAS=$2
export X509_USER_PROXY=$3

echo "This script is based on https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_setup/SMP-RunIILowPUSummer20UL17DR-00008"
voms-proxy-info

export SCRAM_ARCH=slc7_amd64_gcc700

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_30_patch1/src ] ; then
  echo release CMSSW_10_6_30_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_30_patch1
fi
cd CMSSW_10_6_30_patch1/src
eval `scram runtime -sh`
scram b

# cmsDriver command
flist=$(echo `cat ${MINBIAS}`)
echo $flist

cmsDriver.py  --python_filename RAWSIM_cfg.py --eventcontent RAWSIM --pileup "E7TeV_AVE_2_BX2808,{\"N\":3}" --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM-RAW --fileout file:${WORKDIR}/RAWSIM_${SEED}.root --conditions 106X_mc2017_realistic_v9For2017H_v1 --step DIGI:pdigi_valid,L1,DIGI2RAW --geometry DB:Extended --filein file:${WORKDIR}/SIM_${SEED}.root --era Run2_2017 --runUnscheduled --no_exec --mc -n -1 || exit $? ;
echo "fin=open('${MINBIAS}','r'); flist=tuple([l.strip() for l in fin.readlines()]); fin.close(); process.mix.input.fileNames = cms.untracked.vstring(*flist); " >> RAWSIM_cfg.py
cmsRun RAWSIM_cfg.py

cd ${WORKDIR}
