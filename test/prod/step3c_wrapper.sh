#!/bin/bash

WORKDIR=`pwd`
SEED=$1
MINBIAS=$2
export X509_USER_PROXY=$3

echo "This script is based on https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_setup/SMP-RunIILowPUSummer20UL17DR-00008"
voms-proxy-info

export SCRAM_ARCH=slc7_amd64_gcc700

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_30_patch1/src ] ; then
  echo release CMSSW_10_6_30_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_30_patch1
fi
cd CMSSW_10_6_30_patch1/src
eval `scram runtime -sh`
scram b

# cmsDriver command
flist=$(echo `cat ${MINBIAS}`)
echo $flist

# cmsDriver command
cmsDriver.py  --python_filename AODSIM_cfg.py --eventcontent AODSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier AODSIM --fileout file:${WORKDIR}/AODSIM_${SEED}.root --conditions 106X_mc2017_realistic_v9For2017H_v1 --step RAW2DIGI,L1Reco,RECO,RECOSIM,EI --geometry DB:Extended --filein file:${WORKDIR}/HLT_${SEED}.root --era Run2_2017 --runUnscheduled --no_exec --mc -n -1 || exit $? ;
cmsRun AODSIM_cfg.py

cd ${WORKDIR}
