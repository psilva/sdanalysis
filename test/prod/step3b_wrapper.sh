#!/bin/bash

WORKDIR=`pwd`
SEED=$1
MINBIAS=$2
export X509_USER_PROXY=$3

echo "This script is based on https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_setup/B2G-RunIISummer20UL17HLT-00001"
voms-proxy-info

export SCRAM_ARCH=slc7_amd64_gcc630

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_9_4_14_UL_patch1/src ] ; then
  echo release CMSSW_9_4_14_UL_patch1 already exists
else
  scram p CMSSW CMSSW_9_4_14_UL_patch1
fi
cd CMSSW_9_4_14_UL_patch1/src
eval `scram runtime -sh`

cmsDriver.py  --python_filename HLT_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM-RAW --fileout file:${WORKDIR}/HLT_${SEED}.root --conditions 94X_mc2017_realistic_v15 --customise_commands 'process.source.bypassVersionCheck = cms.untracked.bool(True)' --step HLT:2e34v40 --geometry DB:Extended --filein file:${WORKDIR}/RAWSIM_${SEED}.root --era Run2_2017 --no_exec --mc -n -1 || exit $? ;
cmsRun HLT_cfg.py

cd ${WORKDIR}
