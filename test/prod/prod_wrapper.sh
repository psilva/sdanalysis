WORKDIR=`pwd`
CFI=$1
if [ -z "$CFI" ]; then
    echo "Need to provide GEN CFI as argument 1"
    exit $?;
fi
EVENTS=$2
if [ -z "$EVENTS" ]; then
    echo "Need to provide #EVENTS as argument 2"
    exit $?;
fi
SEED=$3
if [ -z "$SEED" ]; then
    echo "Need to provide SEED as argument 3"
    exit $?;
fi
export X509_USER_PROXY=${4}
if [ -z "$X509_USER_PROXY" ]; then
    echo "Need to provide grid PROXY as argument 4"
    exit $?;
fi
OUTDIR=${5}
if [ -z "$OUTDIR" ]; then
    echo "Need to provide output directory as argument 5"
    exit $?;
fi
SCRIPT_DIR=${6}
if [ -z "$SCRIPT_DIR" ]; then
    SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
fi
MINBIAS=${SCRIPT_DIR}/minbias_files

echo "WORKDIR=$WORKDIR"
echo "CFI=$CFI"
echo "#EVENTS=$EVENTS"
echo "SEED=$SEED"
echo "PROXY=$X509_USER_PROXY"
echo "MINBIAS=${MINBIAS}"

sh ${SCRIPT_DIR}/step1_wrapper.sh $CFI $EVENTS $SEED
sh ${SCRIPT_DIR}/step2_wrapper.sh $SEED
sh ${SCRIPT_DIR}/step3a_wrapper.sh $SEED $MINBIAS $X509_USER_PROXY
sh ${SCRIPT_DIR}/step3b_wrapper.sh $SEED $MINBIAS $X509_USER_PROXY
sh ${SCRIPT_DIR}/step3c_wrapper.sh $SEED $MINBIAS $X509_USER_PROXY
sh ${SCRIPT_DIR}/step4_wrapper.sh $SEED

#eos mkdir ${OUTDIR}
#xrdcp MINIAODSIM_${SEED}.root root://eoscms.cern.ch/${OUTDIR}/MINIAODSIM_${SEED}.root
