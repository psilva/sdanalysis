#!/bin/bash


WORKDIR=`pwd`
CFI=$1
EVENTS=$2
SEED=$3

echo "This script is based on https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_setup/SMP-RunIILowPUSummer20UL17GEN-00013"

export SCRAM_ARCH=slc7_amd64_gcc700
source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_30_patch1/src ] ; then
  echo release CMSSW_10_6_30_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_30_patch1
fi
cd CMSSW_10_6_30_patch1/src
eval `scram runtime -sh`


if [[ "${CFI}" == *"raw.githubusercontent.com"* ]]; then
    cfiurl=${CFI} 
else
    cfiurl=https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_fragment/${CFI}
fi
# Download fragment
curl -s -k ${cfiurl} --retry 3 --create-dirs -o Configuration/GenProduction/python/GEN-fragment.py
[ -s Configuration/GenProduction/python/GEN-fragment.py ] || exit $?;
scram b


# cmsDriver command
cmsDriver.py Configuration/GenProduction/python/GEN-fragment.py --python_filename GEN_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN --fileout file:${WORKDIR}/GEN_${SEED}.root --conditions 106X_mc2017_realistic_v9For2017H_v1 --beamspot Realistic25ns13TeVEarly2017Collision --step GEN --geometry DB:Extended --era Run2_2017 --no_exec --mc -n ${EVENTS} --customise_commands "from IOMC.RandomEngine.RandomServiceHelper import RandomNumberServiceHelper ; randSvc = RandomNumberServiceHelper(process.RandomNumberGeneratorService) ; randSvc.populate() ; process.source.firstLuminosityBlock = cms.untracked.uint32(${SEED})" || exit $? ;
cmsRun GEN_cfg.py
cd ${WORKDIR}
ls
