#!/bin/bash

WORKDIR=`pwd`
SEED=$1

echo "This script is based on https://cms-pdmv-prod.web.cern.ch/mcm/public/restapi/requests/get_setup/SMP-RunIILowPUSummer20UL17SIM-00008"

export SCRAM_ARCH=slc7_amd64_gcc700

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_30_patch1/src ] ; then
  echo release CMSSW_10_6_30_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_30_patch1
fi
cd CMSSW_10_6_30_patch1/src
eval `scram runtime -sh`
scram b

# cmsDriver command
cmsDriver.py  --python_filename SIM_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM --fileout file:${WORKDIR}/SIM_${SEED}.root --conditions 106X_mc2017_realistic_v9For2017H_v1 --beamspot Realistic25ns13TeVEarly2017Collision --step SIM --geometry DB:Extended --filein file:${WORKDIR}/GEN_${SEED}.root --era Run2_2017 --no_exec --mc -n -1 || exit $? ;
cmsRun SIM_cfg.py
cd ${WORKDIR}
ls
