#!/bin/bash

outdir=/eos/user/p/psilva/data/sdanalysis/NANO

#MC (submit for xangle=150)
indir=/eos/cms/store/cmst3/group/top/sdanalysis/MC/
python createNanoCondorJDL.py -i ${indir} -o ${outdir} --xangle 150 #-s

#DATA
indir=/eos/cms/store/cmst3/group/top/sdanalysis/Data/
#python createNanoCondorJDL.py -i ${indir} -o ${outdir} -s
