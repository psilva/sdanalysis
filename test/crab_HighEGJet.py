import sys
sys.path.append("./")
from crab_data import config

config.Data.inputDataset = '/HighEGJet/Run2017H-UL2017_MiniAODv2-v1/MINIAOD'
config.Data.unitsPerJob = 10

#v1
#config.General.requestName = 'HighEGJet2017H'
#config.Data.outputDatasetTag = config.General.requestName

#v2
config.General.requestName = 'HighEGJet2017H_v2'
config.Data.outputDatasetTag = config.General.requestName
config.Data.lumiMask = 'crab_projects/crab_HighEGJet2017H/results/notFinishedLumis.json'

#config.Site.blacklist = ['T1_US_FNAL']
