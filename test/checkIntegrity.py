import sys
import os
import ROOT

path=sys.argv[1]

corrupted=[]
for r, d, f in os.walk(path):
    for fp in f:
        if not '.root' in fp: continue
        fp=os.path.join(r, fp)
        try:
            fIn=ROOT.TFile.Open(fp,'READ')
            if fIn is None or fIn.IsZombie() or fIn.TestBit(ROOT.TFile.kRecovered):
                raise ValueError('Corrupted file')
            tree=fIn.Get('Events')
            tree.GetEntriesFast()
            fIn.Close()
        except:
            corrupted.append(fp)

if len(corrupted)>0:
    print('Found {} files'.format(len(corrupted)))
    for fp in corrupted:
        print(fp)
else:
    print('No corrupted files - yay!')
