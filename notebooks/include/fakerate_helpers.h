#ifndef __fakerate_helpers_h__ 
#define __fakerate_helpers_h__ 

#include "TMath.h"
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include <vector>

using namespace ROOT::VecOps; 

using rvec_f = const RVec<float>;

rvec_f muFRetaBins_{0., 0.2778, 0.5556, 0.8333, 1.1111, 1.3889, 1.6667, 1.9444, 2.222, 2.5};
rvec_f eleFRetaBins_{0., 0.2778, 0.5556, 0.8333, 1.1111, 1.3889, 1.6667, 1.9444, 2.222, 2.5};

/**
    @short this function checks which bins are closest to a lepton rapidity and returns a proposed set of 
    pseudo-rapidities and weights to evaluate fake rates from
    the weights are computed with the inverse distance squared
    
    $w(eta_i) = 1/d_i^2 / \sum_k 1 /d_k^2$
    
    with $d_i = \max(\eta - \eta_i,0.01)$ being $\eta$ the lepton pseudorapidity
    and $\eta_i$ the center of the i-th pseudorapidity bin
*/
rvec_f defineLeptonFRetaWeights(const Int_t &pdgId, const Float_t &abseta, const size_t nneighbor=3) {
    
    //binning used to parameterize fake rates
    //this needs to be in synch with fakerate_helpers.py
    //special case for electrons to not mix EB and EE
    bool isEB(abseta<1.45);
    rvec_f etabins( pdgId==11 ?
                    (isEB ? eleFRetaBins_[eleFRetaBins_<1.45] : eleFRetaBins_[eleFRetaBins_>1.45]):
                     muFRetaBins_ );
        
    //bin centers
    size_t netabins(etabins.size());
    rvec_f etacen=0.5*(ROOT::VecOps::Take(etabins,netabins-1)+ROOT::VecOps::Take(etabins,1-netabins));
    
    //distances to bin centers
    rvec_f deta2 = (etacen-abseta)*(etacen-abseta)+1e-10;    
        
    //take the nneighbors closest in delta Eta
    auto sortidx = ROOT::VecOps::Take( ROOT::VecOps::Argsort(deta2), nneighbor);
        
    //find weights, eta of bin centers
    std::vector<float> etawgt(nneighbor*2,0.);
    float wgtSum = ROOT::VecOps::Sum( ROOT::VecOps::Take(1./deta2,sortidx) );
    for(size_t i=0; i<nneighbor; i++) {        
        auto idx=sortidx[i];
        etawgt[i*2]=(1./deta2[idx])/wgtSum;
        etawgt[i*2+1]=etacen[idx];    
    }
    
    //all done
    return etawgt;
}

// tester function
void fakerate_helpers() {
 
    Int_t pdgId=11;
    Float_t abseta=1.6;
    size_t nneighbor=3;
    auto etawgt=defineLeptonFRetaWeights(pdgId,abseta,nneighbor);
    std::cout << "pdgId=" << pdgId << " |eta|=" << abseta << std::endl;
    for(size_t i=0; i<nneighbor; i++) {
        std::cout << i << " w=" << etawgt[i*2] << " etabin=" << etawgt[i*2+1] << std::endl;
    }
 
}


#endif
