import cmsstyle as CMS
import pandas as pd
import numpy as np
import os
import ROOT

def buildCombinedPlotFromDatasets(pname,rootdir,datasets,weights):
    
    """sums up the contributions to a distribution from different dataset files
    pname := the name of the plot to get from the files
    rootdir := the directory with the ROOT files to use
    datasets := a list of datasets to combine in the final histogram
    weights := a list of the weights to apply when scaling the contribution from each dataset
    
    """
    
    #lengths must match
    assert(len(datasets)==len(weights))
    
    #loop over the datasets, read the histogram from the ROOT file and apply the final scaling
    #first ROOT file is used as the reference form the plot, others, are added on top
    plot=None
    for d,w in zip(datasets,weights):
        
        url=f'{rootdir}/{d}.root'
        if not os.path.isfile(url):
            #print(f'Skipping {url}')
            continue
        fIn=ROOT.TFile.Open(url)
        iplot = fIn.Get(pname)
        if plot is None:             
            plot=iplot.Clone()
            plot.Reset('ICE')            
            plot.SetDirectory(0)
        plot.Add(iplot,w)
        fIn.Close()

    return plot
    

def buildPlots(plotlist,rootdir,plot_samples='etc/w_plot_samples.json',lumi=197.,
               sfdict={}):
     
    """this function builds a dict with the data and mc contributions to the final plot
    
    plotlist := list of plots to make
    rootdir  := directory with ROOT files
    plot_samples := json file describing how the samples should be used and final xsec to apply
    lumi := luminosity to use when scaling the MC
        
    """
        
    plot_dict={}
    for p in plotlist:
        plot_dict[p]={'mcstack':[], 'data':None, 'overlay':[]}
    
    #iterate over the samples to use in the plot
    samples = pd.read_json(plot_samples,orient='records')
    for idx,row in samples.iterrows():
        
        #title, list of datasets to combine and weights to apply
        title=row['title']
        datasets=row['datasets']
        weights=lumi*np.array(row['xsec'])
        weights=np.where(weights<0, np.ones_like(weights), weights)
        if row['title'] in sfdict:
            print(f"Scaling {row['title']} by {sfdict[row['title']]}")
            weights *= sfdict[ row['title'] ]

        #get the weighted combined plot, set its title and name, and add to the appropriate list
        for p in plotlist:
            plot=buildCombinedPlotFromDatasets(p,rootdir,datasets,weights)
            if plot is None:
                print(f'Could not retrieve plot for {title} skipping this sample')
                break
            plot.SetTitle(row['title'])
            plot.SetName(f'{plot.GetName()}_{idx}')
            if row['mc']:
                plot_dict[p]['mcstack'].append(plot)
                if row['overlay']:
                    plot_dict[p]['overlay'].append(plot)
            else:
                plot_dict[p]['data'] = plot

    return plot_dict


def showPlot(mcstack=[],data=None,overlay=[],output='./plot',extlist=['png','pdf'],lumitext='197 pb^{-1}',
             maxy=None,miny=None,logy=False,showratio=True,minr=0.5,maxr=2):
    
    #check that there is something to plot
    allplots=[h for h in mcstack+overlay+[data] if not h is None]
    nplots=len(allplots)
    if nplots==0 : return
    
    xlabel=allplots[0].GetXaxis().GetTitle()
    ylabel=allplots[0].GetYaxis().GetTitle()
    minx=allplots[0].GetXaxis().GetXmin()
    maxx=allplots[0].GetXaxis().GetXmax() 
    miny=min([h.GetMinimum() for h in allplots])    
    maxy=max([h.GetMaximum() for h in allplots])*1.3
    if logy:
        miny=max(0.5,miny)
        maxy=2*maxy

    ratio=None
    if showratio:
        mcsum=mcstack[0].Clone('mcsum')
        mcsum.Reset('ICE')
        for h in mcstack: 
            mcsum.Add(h)
        ratio=data.Clone('ratio')
        ratio.Divide(mcsum)
        ratio.SetMarkerStyle(20)
        ratio.SetMarkerColor(1)
        ratio.SetLineColor(1)
        if minr is None:
            minr=min([ratio.GetMinimum()*0.8,minr])
        if maxr is None:
            maxr=max([ratio.GetMaximum()*1.2,maxr])
        mcsum.Delete()
    
    #start the canvas
    CMS.SetExtraText("Preliminary")
    CMS.SetLumi(lumitext)
    CMS.SetEnergy("13")
    CMS.ResetAdditionalInfo()
    c = \
        CMS.cmsDiCanvas('',minx,maxx,miny,maxy,minr,maxr,xlabel,ylabel,'Obs/Exp',square = CMS.kSquare, extraSpace=0.01, iPos=0) \
        if showratio else \
        CMS.cmsCanvas('', minx, maxx, miny, maxy, xlabel, ylabel, square = CMS.kSquare, extraSpace=0.01, iPos=0)

    pupper=c
    if showratio: pupper=c.cd(1)
        
    leg = CMS.cmsLeg(0.6, 0.89 - 0.05*nplots, 0.97, 0.89, textSize=0.035)
    if not data is None:
        leg.AddEntry(data,'Data','ep')
    
    if len(mcstack)>0:
        stack = ROOT.THStack("stack", "stack")
        for i,h in enumerate(mcstack):
            h.SetFillStyle(1001)
            ci=ROOT.TColor.GetColor(CMS.petroff_10[i])
            h.SetFillColor(ci)
            h.SetLineColor(ci)
            stack.Add(h,'hist')
            leg.AddEntry(h,h.GetTitle(),'lf')
        stack.Draw('histsame')
        
    for i,o in enumerate(overlay):
        o.SetFillStyle(0)
        o.SetLineWidth(2)
        ci=ROOT.TColor.GetColor(CMS.petroff_10[i])
        o.SetLineColor(ci)
        o.Draw('histsame')
        leg.AddEntry(o,o.GetTitle(),'l')
    
    if not data is None:
        data.SetMarkerStyle(20)
        data.SetMarkerColor(1)
        data.SetLineColor(1)
        data.Draw('e1same')
        
    if logy:
        pupper.SetLogy()
        
    if showratio:
        plower=c.cd(2)
        ratio.Draw('e1same')
        c.cd()
                     
    ROOT.gPad.RedrawAxis()
    
    #supress the save info messages
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    for ext in extlist:
        c.SaveAs(f'{output}.{ext}')
        
    c.Close()  
    
    
def show2DPlot(h2d,outputname,extlist=['png','pdf'],lumi='197 pb^{-1}',opt='colz',txtfmt='4.2f',overlay=[],logx=False,logy=False,logz=False):
    """2D plot display
    """
    
    xmin=h2d.GetXaxis().GetXmin()
    xmax=h2d.GetXaxis().GetXmax()
    if logx:
        xmin=max(1,h2d.GetXaxis().GetXmin())
    xlab=h2d.GetXaxis().GetTitle()
    ymin=h2d.GetYaxis().GetXmin()
    ymax=h2d.GetYaxis().GetXmax()
    if logy:
        ymin=max(1,h2d.GetYaxis().GetXmin())
    ylab=h2d.GetYaxis().GetTitle()
    canv = CMS.cmsCanvas('c',xmin,xmax,ymin,ymax,xlab,ylab,square=CMS.kSquare,
                         extraSpace=0.01,iPos=0,with_z_axis=True)
    if 'text' in opt:
        ROOT.gStyle.SetPaintTextFormat(txtfmt);
    h2d.Draw(f"same {opt}")
    CMS.SetCMSPalette()
    CMS.SetLumi(lumi)
    CMS.UpdatePalettePosition(h2d, canv)

    nplots=len(overlay)
    if nplots>0:
        if nplots>1:
            leg = CMS.cmsLeg(0.15,0.92, 0.4, 0.92-nplots*0.05, textSize=0.035)
        for i,o in enumerate(overlay):
            o.SetMarkerStyle(20)
            ci=ROOT.TColor.GetColor(CMS.petroff_10[i])
            o.SetMarkerColor(ci)
            o.SetLineColor(ci)
            o.SetFillStyle(0)
            o.Draw('p same')
            if nplots>1:
                leg.AddEntry(o,o.GetTitle(),'ep')
        if nplots>1:
            leg.Draw()

    if logx:
        canv.SetLogx()        
    if logy:
        canv.SetLogy()
    if logz:
        canv.SetLogz()
    
    for ext in ['png','pdf']:
        canv.SaveAs(f'{outputname}.{ext}')
    canv.Close()
    
def fitGraph(gr,func,opt,cl=0.68):
    """wraps the procedure of fitting a function (`func`) to a graph (`gr`) and retrieving the confidence intervals
    """
    ngr=gr.GetN()
    gr.Fit(func,opt)
    grint = ROOT.TGraphErrors(ngr)
    for i in range(ngr):
        grint.SetPoint(i, gr.GetX()[i], 0)
    ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(grint,cl);
    return grint

def graphOp(gr1,gr2,op='diff'):
    
    assert(gr1.GetN()==gr2.GetN())
    
    gr_op=ROOT.TGraphErrors()
    for i in range(gr1.GetN()):
        
        y1=gr1.GetY()[i]
        y2=gr2.GetY()[i]
        if op=='ratio' and y2==0 : continue
        
        res=y1/y2 if op=='ratio' else y1-y2
               
        y1_unc=gr1.GetErrorY(i)
        y2_unc=gr2.GetErrorY(i)
        
        res_unc= np.hypot(y1*y2_unc,y2*y1_unc)/(y2**2) if op=='ratio' \
            else np.hypot(y1_unc,y2_unc)
        
        x=gr2.GetX()[i]
        x_unc=gr2.GetErrorX(i)
        
        j=gr_op.GetN()
        gr_op.SetPoint(j,x,res)
        gr_op.SetPointError(j,x_unc,res_unc)
        
    return gr_op
    
    
    