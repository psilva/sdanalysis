import ROOT

def declareMixerHelpers(urllist):
    ROOT.gROOT.ProcessLine('#include "include/pumixer.h"')
    ROOT.m_ppm.init(urllist)

    
def mixProtons(df):
    
    """handles the mixing of protons on MC redefining the reconstructed proton counters and kinematics when needed
    it assumes that declareMixerHelpers has been called previously
    has already been called so that the ROOT.ppm is available
    """
    
    #move current protons to signal simulated
    df = df.Define('nSignalProtons_neg','nGoodProtons_neg') \
           .Define('nSignalProtons_pos','nGoodProtons_pos') \
           .Define('nSignalProtons',    'nGoodProtons') \
           .Define('SignalProton_xi',  'Proton_xi') \
           .Define('SignalProton_sgn', 'Proton_sgn') \
           .Define('SignalProton_pz',  'Proton_pz') \
           .Define('SignalProton_xisgn','Proton_sgn*Proton_xi')
    
    #add pileup protons
    df = df.Define('PuProton_xisgn','m_ppm.getPUProton()') \
           .Define('PuProton_xi','abs(PuProton_xisgn)' ) \
           .Define('PuProton_sgn','(PuProton_xisgn==0 ? 0. : (PuProton_xisgn>0 ? +1. : -1.))' ) \
           .Define('PuProton_pz', '6500.*PuProton_xisgn')
    
    #reassign reconstructed protons in the presence of pileup
    df = df.Redefine('Proton_xisgn','m_ppm.reassignRecoProton(PuProton_xisgn,SignalProton_xisgn)') \
           .Redefine('Proton_xi','abs(Proton_xisgn)') \
           .Redefine('Proton_sgn','(Proton_xisgn==0 ? 0. : (Proton_xisgn>0 ? +1. : -1.))') \
           .Redefine('Proton_pz','6500.*Proton_xisgn') \
           .Redefine('nGoodProtons_neg','Proton_sgn<0 ? 1 :0') \
           .Redefine('nGoodProtons_pos','Proton_sgn>0 ? 1 :0') \
           .Redefine('nGoodProtons','nGoodProtons_neg+nGoodProtons_pos')
        
    return df

def main():
    
        #init the mixer
        flist={
            "~/www/sdanalysis/2024Feb11/pu_protons/HighEGJet_2017H.root",
            "~/www/sdanalysis/2024Feb11/pu_protons/SingleMuon_2017H.root"
        }
        declareMixerHelpers(flist)
              
        #run on a few events
        df=ROOT.RDataFrame('Events','/eos/user/p/psilva/data/sdanalysis/Wlnu_pomflux_TuneCP5_lowPU_13TeV-pythia8.root')
        df=df.Range(0,1000)   
        df=df.Define('Proton_xisgn','Proton_sgn*Proton_xi')
        df=mixProtons(df)
        df.Snapshot('Events','test.root','(SignalProton.*|PuProton.*|Proton.*|n.*Proton.*)')

if __name__ == '__main__':
    main()
 
