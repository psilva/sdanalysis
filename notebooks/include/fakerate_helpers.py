import ROOT
from array import array
import numpy as np
import os

#eta binning for electrons : EB vs EE 
#                muons     : different tracker/muon chamber regions
fr_etabins={
    11:array('f',np.linspace(0,2.5,10)),
    13:array('f',np.linspace(0,2.5,10))
}
fr_ptbins=array('f',[20,28,36,42,60,80,120])
fr_wgtfunc = ROOT.TF1('logpol','[0]*log(x)*log(x)+[1]*log(x)+[2]',20,1e4)


def fillPuProtonsTree(input,output,ctrlsel='W_mT<20'):

    """selects the sidband and save the information on the protons for mixing purposes"""
    
    ROOT.ROOT.EnableImplicitMT()
    df=ROOT.RDataFrame('Events',input) 
    df = df.Filter(ctrlsel) \
           .Define('Proton_xisgn','Proton_sgn*Proton_xi')
    df.Snapshot('PUProtons',output,'(PV_npvsGood.*|Proton_xisgn.*)')
    print(df.Count().GetValue(),'@',output)


def fillFakeRateDistributions(input,output,mc,sumw,ctrlsel='W_mT<20'):
    
    """helper function to build the fake rate base distributions for a given sample
    The plots will be stored in a root file
    """
    
    ROOT.ROOT.EnableImplicitMT()

    df=ROOT.RDataFrame('Events',input)    
    
    #base weight
    df=df.Define('wgt',f'genWeight*PU_wgt*Trig_Mu*Trig_Ele*Muon_rec*Muon_id*Muon_iso*Electron_idiso*L1PreFiringWeight_Nom/{sumw}' if mc else '1') \
         .Define('Lepton1_abseta','abs(Lepton1_eta)') \
    
    hists=[]
    for tag,sel in [ ('loose_wm', f'(Cat==24 || Cat==24000) && Flav==13'),
                     ('tight_wm', f'Cat==24 && Flav==13'),
                     ('loose_we', f'(Cat==24 || Cat==24000) && Flav==11'),
                     ('tight_we', f'Cat==24 && Flav==11')]:
        
        #fake rate histogram 
        absetabins=fr_etabins[11] if tag[-2:]=='we' else fr_etabins[13]
        nabsetabins=len(absetabins)-1
        nptbins=len(fr_ptbins)-1
        
        #selection of the region
        tag_df = df.Filter(sel)
        
        #selection of the control region used to derive the fake rate
        tag_fr_df = tag_df.Filter(ctrlsel)
                
        hists += [
            tag_df.Histo1D( (f"{tag}_mt", f";Tranverse mass [GeV];Events",50,0,120), 
                           "W_mT", 'wgt'),
            tag_fr_df.Histo1D( (f"{tag}_iso",";Relative isolation;Events",50,0,0.5),
                               'Lepton1_iso', 'wgt'),
            tag_fr_df.Histo2D( (f"{tag}_ptvseta",";Pseudo-rapidity; Transverse momentum [GeV]",nabsetabins,absetabins,nptbins,fr_ptbins),
                               'Lepton1_abseta', 'Lepton1_pt', 'wgt'),
        ]
        
        #n-1 distributions
        for i in range(nabsetabins):
            etasel=f'Lepton1_abseta>={absetabins[i]} && Lepton1_abseta<{absetabins[i+1]}'
            hists += [
                tag_df.Filter(etasel) \
                      .Histo1D( (f"{tag}_mt_{i}", f";Tranverse mass [GeV];Events",25,0,120), 
                                 "W_mT", 'wgt'),
                tag_fr_df.Filter(etasel) \
                      .Histo1D( (f"{tag}_iso_{i}",";Relative isolation;Events",25,0,0.5),
                               'Lepton1_iso', 'wgt'),    
            ]
        
    fOut=ROOT.TFile.Open(output,'RECREATE')
    for h in hists:
        h.Write()
    fOut.Close()
    print(f'Wrote fake rate histograms to {output}')
    
    ROOT.ROOT.DisableImplicitMT()
    

def prepareRatePlots(data,stack,name):
    
    """helper function for the final rate plots to be produced"""
    
    frplots={
        'mc':stack[0].Clone(f'mc_{name}'),
        'dataraw':data.Clone(f'dataraw_{name}'),
        'data':data.Clone(f'data_{name}'),     
        'data_up':data.Clone(f'dataup_{name}'),        
        'data_dn':data.Clone(f'datadn_{name}')
    }
    
    for i in range(1,len(stack)): 
        frplots['mc'].Add( stack[i] )
    
    frplots['data'].Add(frplots['mc'],-1)
    frplots['data_up'].Add(frplots['mc'],-0.7)
    frplots['data_dn'].Add(frplots['mc'],-1.3)
    
    frplots['purity'] = frplots['mc'].Clone(f'purity_{name}')
    frplots['purity'].Divide(frplots['dataraw'])
    
    return frplots

def computeFinalRatePlots(url,fs):
    
    """opens a summary ROOT file containing all the plots produced by `prepareRatePlots` for the loose and tight regions.
    Performs the necessary operations to compute the final ratios used to compute the prompt and fake rates
    The weights for the "loose, not tight" and "tight" counts are also computed
        - loose, not tight := wlnt = fp/(p-f)   
        - tight: wt = f(1-p)(p-f) = wlnt * (1-p)/p
    """
    

    def _ratioFromProjY(h2num,h2den,ibin):
    
        """helper to project and take ratio in a given slice of a 2D histogram"""

        #fill a graph with the ratio of h2num/h2den @ ibin
        gr = ROOT.TGraphAsymmErrors()
        h1num=h2num.ProjectionY('num',ibin,ibin)
        h1den=h2den.ProjectionY('den',ibin,ibin)
        gr.Divide(h1num,h1den,'n')
        h1num.Delete()
        h1den.Delete()
        return gr
    
    outname=url.replace('.root',f'_{fs}.root')
    fOut=ROOT.TFile.Open(outname,'RECREATE')

    #read one of the 2D histograms stored and use it as template
    fIn=ROOT.TFile.Open(url)
    templ=fIn.Get(f'data_loose_{fs}')
        
    for ibin in range(1,templ.GetNbinsX()+1):
        
        fIn.cd()
        
        #prompt rate
        pgr=_ratioFromProjY(fIn.Get(f'mc_tight_{fs}'),fIn.Get(f'mc_loose_{fs}'),ibin)
        
        #fake rate (uncorrected)
        frawgr=_ratioFromProjY(fIn.Get(f'dataraw_tight_{fs}'),fIn.Get(f'dataraw_loose_{fs}'),ibin)
        
        #fake rate (corrected)
        fgr=_ratioFromProjY(fIn.Get(f'data_tight_{fs}'),fIn.Get(f'data_loose_{fs}'),ibin)
        fupgr=_ratioFromProjY(fIn.Get(f'dataup_tight_{fs}'),fIn.Get(f'dataup_loose_{fs}'),ibin)
        fdngr=_ratioFromProjY(fIn.Get(f'datadn_tight_{fs}'),fIn.Get(f'dataup_loose_{fs}'),ibin)
        ftotgr=fgr.Clone()
        
        #final weights
        wlntgr=ROOT.TGraphErrors()
        wtgr=ROOT.TGraphErrors()
        
        #build the fake rate graph with the total (stat+syst) uncertainty
        for k in range(ftotgr.GetN()):
            x=fgr.GetX()[k]
            dx=fgr.GetErrorX(k)
            ycen=fgr.GetY()[k]
            ystat=fgr.GetErrorY(k)
            yup=fupgr.GetY()[k]
            ydn=fdngr.GetY()[k]            
            ddn=np.hypot(ydn-ycen,ystat)
            dup=np.hypot(yup-ycen,ystat)
            dy=0.5*(ddn+dup)
            ftotgr.SetPointError(k,dx,dx,dy,dy)
            
            #weights (we ignore the uncertainty on the purity)
            pval=min(pgr.GetY()[k],1)            
            wlnt = ycen*pval/(pval-ycen) if pval>ycen else 0.
            wlnt_unc = ((wlnt/ycen)**2)*dy if ycen>0. else 0.
            wlntgr.SetPoint(k,x,wlnt)
            wlntgr.SetPointError(k,dx,wlnt_unc)

            wt = wlnt*(1-pval)/pval
            wt_unc = wlnt_unc*(1-pval)/pval
            wtgr.SetPoint(k,x,wt)
            wtgr.SetPointError(k,dx,wt_unc)
            
        #save graphs to file
        fOut.cd()
        pgr.Write(f'p_{ibin}')
        frawgr.Write(f'fraw_{ibin}')
        fgr.Write(f'fstat_{ibin}')
        ftotgr.Write(f'f_{ibin}')
        wlntgr.Write(f'wlnt_{ibin}')
        wtgr.Write(f'wt_{ibin}')
      
    fOut.cd()
    axis=templ.GetXaxis().Clone('xaxis')
    axis.Write()
    fOut.Close()
    fIn.Close()
    
    return outname
        
    