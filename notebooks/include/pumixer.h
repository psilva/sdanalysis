#ifndef __pumixer_h__ 
#define __pumixer_h__ 

#include "TString.h"
#include "TChain.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>


/**
    @short this class loads a vector of pileup proton candidates to memory,
    shuffles its order and returns on demand a new pileup proton candidate
*/
class PileupProtonMixer {
  
    public:
    
        PileupProtonMixer() = default;
    
        PileupProtonMixer(PileupProtonMixer &other) {
            xi_=other.xi_;
            i_=other.i_;
            nxi_=other.nxi_;
        }
    
        /**
            @short load values from tree stored in ROOT file and shuffle its order
        */
        void init(std::vector<TString> url) {
            
            //fill the vector of xi measurements
            TChain *chain=new TChain("PUProtons");
            for(auto f : url) chain->AddFile(f);
            
            Float_t xisgn;
            chain->SetBranchAddress("Proton_xisgn",&xisgn);
            for(unsigned int i=0; i<chain->GetEntries(); i++) {
                chain->GetEntry(i);
                xi_.push_back(xisgn);
            }
            std::cout << "Loaded " << xi_.size() << " pileup proton candidates" << std::endl;
            
            //shuffle the vector of xi to mix
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(xi_.begin(), xi_.end(), g);
            nxi_=xi_.size();
            
            //current candidate
            i_=nxi_;
        }
    
        /**
            @short return new pileup proton candidate on demand
        */
        Float_t getPUProton() {

            i_++;
            if(i_>=nxi_) i_=0;
            return xi_[i_];
        }
    
        /**
            @short implements the pileup proton mixing logic
        */
        Float_t reassignRecoProton(const Float_t &PuProton_xisgn,const Float_t &Proton_xisgn) {            
            Float_t finalxi(0.); //proton is killed
            if(PuProton_xisgn!=0. && Proton_xisgn!=0.) finalxi=0.;
            else if(PuProton_xisgn!=0.) finalxi=PuProton_xisgn;
            else if(Proton_xisgn!=0.) finalxi=Proton_xisgn;
            return finalxi;            
        }

    
        ~PileupProtonMixer() {
            xi_.clear();
        }
        
    private:
        size_t i_,nxi_;
        std::vector<Float_t> xi_;
};

PileupProtonMixer m_ppm;


#endif
